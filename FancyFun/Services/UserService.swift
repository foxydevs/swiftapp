//
//  UserService.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 8/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class UserService {
    func doLogin(parameters:JSON,_ callback:@escaping (JSON, Bool) -> Void){
        let urlString: String = Constants.urlBase + "/login"
        Alamofire.request(urlString, method: .post, parameters: parameters.dictionaryObject, encoding: JSONEncoding.default)
            .response { (response) in
                let json = JSON(data: response.data!)
                print(response.error)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func newUser(parameters: JSON,_ callback:@escaping (JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/users"
        Alamofire.request(urlString, method: .post, parameters: parameters.dictionaryObject, encoding: JSONEncoding.default)
            .response { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func loginFacebook(parameters: JSON,_ callback:@escaping (JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/users/new/facebook"
        Alamofire.request(urlString, method: .post, parameters: parameters.dictionaryObject, encoding: JSONEncoding.default)
            .response { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func updateUser(id: String, parameters: JSON,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/users/\(id)"
        print(urlString)
        print(parameters)
        Alamofire.request(urlString, method: .put, parameters: parameters.dictionaryObject, encoding: JSONEncoding.default)
            .response { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, "")
                } else {
                    status = false
                    callback(json, status, response.error.debugDescription)
                }
        }
    }
    
    func updateAvatar(id: String, photo: UIImage,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/upload/avatar/\(id)"
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(UIImageJPEGRepresentation(Singleton.shared.fixOrientation(imageChange: photo), 0.5)!, withName: "avatar", fileName: "jpeg", mimeType: "image/jpeg")
        }, to: urlString) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON{ response in
                    let json = JSON(data: response.data!)
                    callback(json, true, response.description)
                }
            case .failure(let encodingError):
                print("FAILURE")
                print(encodingError)
                callback([:], false, encodingError.localizedDescription)
            }
        }
    }
    
    func getUser(id: String,_ callback:@escaping (JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/users/\(id)"
        Alamofire.request(urlString)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func getUserProfile(friend: Int, user: String,_ callback:@escaping(JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/users/\(friend)/friends/\(user)"
        Alamofire.request(urlString)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func getInvitation(id: String,_ callback:@escaping(JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/users/\(id)/invitations"
        Alamofire.request(urlString)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
}
