//
//  User.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

class User: NSObject {
    var id: Int
    var username: String
    var email: String
    var firstname: String
    var lastname: String
    var work: String
    var userDescription: String
    var age: String
    var birthday: String
    var phone: String
    var picture: String
    var last_connection: String
    var facebook_id: String
    var last_latitud: Float
    var last_longitud: Float
    var created_at: String
    var updated_at: String
    var distance: Double
    var similar: [User] = []
    var interests: [Interest] = []
    var numberEventsCreated: Int
    var numberEventsAssisted: Int
    
    init(json: JSON) {
        id = json["id"].intValue
        username = json["username"].stringValue
        email = json["email"].stringValue
        firstname = json["firstname"].stringValue
        lastname = json["lastname"].stringValue
        work = json["work"].stringValue
        userDescription = json["description"].stringValue
        age = json["age"].stringValue
        birthday = json["birthday"].stringValue
        phone = json["phone"].stringValue
        picture = json["picture"].stringValue
        last_connection = json["last_conection"].stringValue
        facebook_id = json["facebook_id"].stringValue
        last_latitud = json["last_latitud"].floatValue
        last_longitud = json["last_longitud"].floatValue
        created_at = json["created_at"].stringValue
        updated_at = json["updated_at"].stringValue
        distance = Double(round(100*json["distance"].doubleValue)/100)
        numberEventsCreated = json["number_events_created"].intValue
        numberEventsAssisted = json["number_events_assisted"].intValue
        
        for oSimilar: JSON in json["similar"].arrayValue {
            similar.append(User(json: oSimilar))
        }
        
        for interest: JSON in json["interests"].arrayValue {
            interests.append(Interest(json: interest))
        }
    }
    
    func getFullName() -> String {
        return "\(self.firstname) \(self.lastname)"
    }
    
    func getAvatarURL() -> URL {
        return URL(string: self.picture)!
    }
}
