//
//  MenuViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 8/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import AlamofireImage
import Material
import FBSDKCoreKit
import FBSDKLoginKit

class MenuViewController: UIViewController {

    @IBOutlet weak var userAvatar: UIImageView?
    @IBOutlet weak var userName: UILabel?
    @IBOutlet weak var userStatus: UILabel?
    @IBOutlet weak var tableMenu: UITableView?
    
    let optionsTitle = [
        NSLocalizedString("menu_activities", comment: ""),
        NSLocalizedString("menu_profile", comment: ""),
        NSLocalizedString("menu_closefriends", comment: ""),
        NSLocalizedString("menu_friendlist", comment: ""),
        NSLocalizedString("menu_exit", comment: "")
        ]
    
    let optionsIcons = ["activities", "profile", "closefriends", "friendlist", "exit"]
    let optionsNumber = 5
    var optionsActive = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableMenu?.dataSource = self
        tableMenu?.delegate = self
        
        tableMenu?.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .none)
        
        loadLayout()
    }
    
    func loadLayout() -> Void {
        userAvatar?.af_setImage(withURL: URL(string: Singleton.shared.pictureURL)!)
        userAvatar?.layer.cornerRadius = (userAvatar?.frame.size.height)! / 2
        userAvatar?.layer.masksToBounds = true
        userName?.text = Singleton.shared.fullName
    }
    
    func logout() -> Void {
        let preferences = UserDefaults.standard
        preferences.removeObject(forKey: Constants.keyId)
        preferences.removeObject(forKey: Constants.keyFirstName)
        preferences.removeObject(forKey: Constants.keyLastName)
        preferences.removeObject(forKey: Constants.keyPicture)
        
        let manager = FBSDKLoginManager()
        manager.logOut()
        
        FBSDKAccessToken.setCurrent(nil)
        FBSDKProfile.setCurrent(nil)
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension MenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionsNumber
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        cell.name?.text = optionsTitle[indexPath.row]
        cell.iconName = optionsIcons[indexPath.row]
        
        return cell
    }
}

extension MenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == optionsActive {
            sideMenuController?.toggle()
        } else {
            self.optionsActive = indexPath.row
            switch indexPath.row {
            case 0:
                let vcE = self.storyboard?.instantiateViewController(withIdentifier: "EventsView") as! EventsViewController
                vcE.title = NSLocalizedString("menu_near", comment: "")
                vcE.pageTabBarItem.title = NSLocalizedString("menu_near", comment: "")
                vcE.pageTabBarItem.titleColor = Constants.tabBarText
                vcE.friendEvents = false
                let vcF = self.storyboard?.instantiateViewController(withIdentifier: "EventsView") as! EventsViewController
                vcF.title = NSLocalizedString("menu_friends", comment: "")
                vcF.pageTabBarItem.title = NSLocalizedString("menu_friends", comment: "")
                vcF.pageTabBarItem.titleColor = Constants.tabBarText
                vcF.friendEvents = true
                sideMenuController?.embed(centerViewController: UINavigationController(rootViewController: AppPageTabBarController(viewControllers: [vcE, vcF])))
                break
            case 1:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileView") as! ProfileViewController
                vc.title = NSLocalizedString("menu_profile", comment: "")
                sideMenuController?.embed(centerViewController: UINavigationController(rootViewController: vc))
                break
            case 2:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CloseFriendsView") as! CloseFriendsViewController
                vc.title = NSLocalizedString("menu_closefriends", comment: "")
                sideMenuController?.embed(centerViewController: UINavigationController(rootViewController: vc))
                break
            case 3:
                let vcE = self.storyboard?.instantiateViewController(withIdentifier: "FriendsListView") as! FriendsListViewController
                vcE.title = NSLocalizedString("menu_friendlist", comment: "")
                vcE.pageTabBarItem.title = NSLocalizedString("menu_friendlist", comment: "")
                vcE.pageTabBarItem.titleColor = Constants.tabBarText
                let vcF = self.storyboard?.instantiateViewController(withIdentifier: "InvitationsView") as! InvitationsViewController
                vcF.title = NSLocalizedString("menu_invitations", comment: "")
                vcF.pageTabBarItem.title = NSLocalizedString("menu_invitations", comment: "")
                vcF.pageTabBarItem.titleColor = Constants.tabBarText
                sideMenuController?.embed(centerViewController: UINavigationController(rootViewController: FriendsTabBarController(viewControllers: [vcE, vcF])))
                break
            case 4:
                self.logout()
                break
            default:
                break
            }
        }
    }
}
