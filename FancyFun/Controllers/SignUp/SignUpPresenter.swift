//
//  SignUpPresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 8/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SignUpView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func successSignUp()
    func failSignUp()
}

class SignUpPresenter {
    fileprivate let userService: UserService
    weak fileprivate var signUpView: SignUpView?
    
    init(userService: UserService) {
        self.userService = userService
    }
    
    func attachView(_ view: SignUpView) {
        signUpView = view
    }
    
    func detachView() {
        signUpView = nil
    }
    
    func newUser(parameters: JSON) {
        signUpView?.startLoading()
        userService.newUser(parameters: parameters) { (json, status) in
            self.signUpView?.finishLoading()
            if status {
                print(json)
                self.signUpView?.successSignUp()
            } else {
                print(json)
                self.signUpView?.failSignUp()
            }
        }
    }
    
    func doLoginFacebook(parameters: JSON) -> Void {
        signUpView?.startLoading()
        userService.loginFacebook(parameters: parameters) { (user, status) in
            self.signUpView?.finishLoading()
            if status {
                let preferences = UserDefaults.standard
                let valueId = user["id"].stringValue
                let valueFirstName = user["firstname"].stringValue
                let valueLastName = user["lastname"].stringValue
                let valuePicture = user["picture"].stringValue
                preferences.set(valueId, forKey: Constants.keyId)
                preferences.set(valueFirstName, forKey: Constants.keyFirstName)
                preferences.set(valueLastName, forKey: Constants.keyLastName)
                preferences.set(valuePicture, forKey: Constants.keyPicture)
                preferences.set(0, forKey: Constants.keyDistance)
                preferences.synchronize()
                
                Singleton.shared.loadUser()
                
                self.signUpView?.successSignUp()
            } else {
                self.signUpView?.failSignUp()
            }
        }
    }
}
