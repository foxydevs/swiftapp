//
//  InterestViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 3/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftyJSON

class InterestViewController: UIViewController {

    @IBOutlet weak var tableInterest: UITableView?
    
    fileprivate let interestPresenter = InterestPresenter(interestService: InterestService())
    fileprivate var myInterest: [JSON] = [];
    fileprivate var otherInterests: [JSON] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("text_interest", comment: "")
        tableInterest?.delegate = self
        tableInterest?.dataSource = self
        interestPresenter.attachView(self)
        
        interestPresenter.getInterest(id: Singleton.shared.userID)
    }
    
    func showAlert(indexPath: IndexPath) -> Void {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: (indexPath.section == 0 ? NSLocalizedString("text_deleteinterest", comment: "") : NSLocalizedString("text_addinterest", comment: "")), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("yes_text", comment: ""), style: .default, handler: { (UIAlertAction) in
            if indexPath.section == 0 {
                self.deleteInterest(index: indexPath.row)
            } else {
                self.addInterest(index: indexPath.row)
            }
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("no_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func addInterest(index: Int) -> Void {
        interestPresenter.createInterest(parameters: [ "user": Singleton.shared.userID, "name": otherInterests[index]["name"].stringValue ])
    }
    
    func deleteInterest(index: Int) -> Void {
        interestPresenter.deleteInterest(id: myInterest[index]["id"].intValue)
    }
}

extension InterestViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.myInterest.count
        } else {
            return self.otherInterests.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomInterestCell", for: indexPath) as! CustomInterestCell
        cell.count?.text = "0"
        cell.count?.isHidden = true
        if indexPath.section == 0 {
            let interest = myInterest[indexPath.row]
            cell.name?.text = interest["name"].stringValue
        } else {
            let interest = otherInterests[indexPath.row]
            cell.name?.text = interest["name"].stringValue
            cell.count?.text = interest["user_count"].stringValue
            cell.count?.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return NSLocalizedString("text_interest", comment: "")
        } else {
            return NSLocalizedString("text_otherinterest", comment: "")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        showAlert(indexPath: indexPath)
    }
}

extension InterestViewController: InterestView {
    func startLoading() {
        KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
    }
    
    func finishLoading() {
        KRProgressHUD.dismiss()
    }
    
    func setInterests(interest: JSON) {
        self.myInterest = interest["myInterest"].arrayValue
        self.otherInterests = interest["otherInterests"].arrayValue
        self.tableInterest?.reloadData()
    }
    
    func reloadInterests() {
        interestPresenter.getInterest(id: Singleton.shared.userID)
    }
    
    func failRequest(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_user", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
