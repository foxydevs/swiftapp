//
//  CloseFriendsViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 12/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftyJSON
import AlamofireImage

class CloseFriendsViewController: UIViewController {

    @IBOutlet weak var tableCloseFriends: UITableView?
    let refreshControl = UIRefreshControl()
    var friends: [User] = []
    var nearpeople: [User] = []
    var selectedRow = -1
    fileprivate let closeFriendsPresenter = CloseFriendsPresenter(friendsService: FriendsService(), userService: UserService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableCloseFriends?.delegate = self
        tableCloseFriends?.dataSource = self
        tableCloseFriends?.tableFooterView = UIView(frame: .zero)
        setupTableView()
        closeFriendsPresenter.attachView(self)
        getEventsWhenLocation()
    }
    
    func setupTableView() -> Void {
        // Helpers
        let attributes = [ NSForegroundColorAttributeName : Constants.baseColor ] as [String: Any]
        
        refreshControl.tintColor = Constants.baseColor
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("refresh_closer", comment: ""), attributes: attributes)
        refreshControl.addTarget(self, action: #selector(refreshEvents(sender:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableCloseFriends?.refreshControl = refreshControl
        } else {
            tableCloseFriends?.addSubview(refreshControl)
        }
    }
    
    func getEventsWhenLocation() -> Void {
        let parameters: JSON = ["id": Singleton.shared.userID,"latitude": Singleton.shared.currentLocation?.coordinate.latitude ?? 0.0,"longitude": Singleton.shared.currentLocation?.coordinate.longitude ?? 0.0, "distance": Singleton.shared.distance]
        closeFriendsPresenter.getNearPeople(parameters: parameters)
    }
    
    func refreshEvents(sender: UIRefreshControl) -> Void {
       getEventsWhenLocation()
    }

    @IBAction func sendMessage(sender: UIButton) -> Void {
        
    }
    
    @IBAction func sendInvitattion(sender: UIButton) -> Void {
        let parameters: JSON = ["user_send": Singleton.shared.userID, "user_receipt": sender.tag]
        print(parameters)
        closeFriendsPresenter.sendInvitation(parameters: parameters)
    }
}

extension CloseFriendsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return friends.count
        } else {
            return nearpeople.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CloseFriendsCell", for: indexPath) as! CloseFriendsCell
        
        var friend: User
        if indexPath.section == 0 {
            friend = friends[indexPath.row]
        } else {
            friend = nearpeople[indexPath.row]
        }
        
        cell.lblDistance?.text = "\(friend.distance) Km"
        cell.lblName?.text = friend.getFullName()
        cell.lblDescription?.text = friend.userDescription
        cell.lblAge?.text = friend.age
        cell.avatar?.af_setImage(withURL: friend.getAvatarURL())
        cell.btnMessage?.tag = friend.id
        cell.btnInvitattion?.tag = friend.id
        cell.btnMessage?.addTarget(self, action: #selector(sendMessage(sender:)), for: .touchUpInside)
        cell.btnInvitattion?.addTarget(self, action: #selector(sendInvitattion(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Amigos"
        } else {
            return "Personas Cercanas"
        }
    }
}

extension CloseFriendsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1 {
            if selectedRow == indexPath.row {
                return 155
            } else {
                return 115
            }
        } else {
            return 115
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.beginUpdates()
        if indexPath.section == 1 {
            if selectedRow == indexPath.row {
                selectedRow = -1
            } else {
                selectedRow = indexPath.row
            }
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileView") as! UserProfileViewController
            vc.user = friends[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        tableView.endUpdates()
    }
}

extension CloseFriendsViewController: CloseFriendsView {
    func startLoading() {
        if !refreshControl.isRefreshing {
            KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
        }
    }
    
    func finishLoading() {
        refreshControl.endRefreshing()
        KRProgressHUD.dismiss()
    }
    
    func reloadFriends() {
        getEventsWhenLocation()
    }
    
    func setFriends(friends: JSON) {
        self.friends.removeAll()
        for friend: JSON in friends["nearfriends"].arrayValue {
            self.friends.append(User(json: friend))
        }
        self.nearpeople.removeAll()
        for friend: JSON in friends["nearpeople"].arrayValue {
            self.nearpeople.append(User(json: friend))
        }
        tableCloseFriends?.reloadData()
    }
    
    func failRequest(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
