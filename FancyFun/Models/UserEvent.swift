//
//  UserEvent.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 10/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserEvent: NSObject {
    var id: Int
    var state: Int
    var event: Int
    var user: User
    var created_at: String
    var updated_at: String
    
    init(json: JSON) {
        self.id = json["id"].intValue
        self.state = json["state"].intValue
        self.user = User(json: json["user"])
        self.event = json["event"].intValue
        self.created_at = json["created_at"].stringValue
        self.updated_at = json["updated_at"].stringValue
    }
}
