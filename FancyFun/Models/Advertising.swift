//
//  Advertising.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 23/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//
import Foundation
import SwiftyJSON

class Advertising: NSObject {
    var id: Int
    var place: String
    var descriptionAdvertising: String
    var picture: String
    var client: String
    var begin: String
    var end: String
    var state: Int
    var created_at: String
    var updated_at: String
    
    init(json: JSON) {
        self.id = json["id"].intValue
        self.place = json["created_at"].stringValue
        self.descriptionAdvertising = json["descriptionAdvertising"].stringValue
        self.picture = json["picture"].stringValue
        self.client = json["client"].stringValue
        self.begin = json["begin"].stringValue
        self.end = json["end"].stringValue
        self.state = json["state"].intValue
        self.created_at = json["created_at"].stringValue
        self.updated_at = json["updated_at"].stringValue
    }
}
