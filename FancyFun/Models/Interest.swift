//
//  Interest.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

class Interest: NSObject {
    var id: Int
    var name: String
    var user: Int
    var created_at: String
    var updated_at: String
    
    init(json: JSON) {
        self.id = json["id"].intValue
        self.name = json["name"].stringValue
        self.user = json["user"].intValue
        self.created_at = json["created_at"].stringValue
        self.updated_at = json["updated_at"].stringValue
    }
}
