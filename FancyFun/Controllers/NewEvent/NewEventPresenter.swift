//
//  NewEventPresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 27/07/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol NewEventView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setEvent(event: JSON)
    func finishUploadPicture()
    func updateEvent(event: JSON)
    func failRequest(message: String)
}

class NewEventPresenter {
    fileprivate let eventsService: EventsService
    weak fileprivate var newEventsView: NewEventView?
    
    init(eventsService: EventsService) {
        self.eventsService = eventsService
    }
    
    func attachView(_ view: NewEventView) {
        newEventsView = view
    }
    
    func detachView() {
        newEventsView = nil
    }
    
    func createEvent(parameters: JSON) -> Void {
        newEventsView?.startLoading()
        eventsService.createEvent(parameters: parameters) { (event, status, message) in
            if status {
                self.newEventsView?.setEvent(event: event)
            } else {
                self.newEventsView?.failRequest(message: message)
            }
        }
    }
    
    func updateEvent(id: Int, parameters: JSON) -> Void {
        newEventsView?.startLoading()
        eventsService.updateEvent(id: id, parameters: parameters) { (event, status, message) in
            if status {
                self.newEventsView?.updateEvent(event: event)
            } else {
                self.newEventsView?.failRequest(message: message)
            }
        }
    }
    
    func uploadPicture(id: Int, photo: UIImage) -> Void{
        eventsService.insertPictureEvent(id: id, photo: photo) { (json, status, message) in
            self.newEventsView?.finishLoading()
            if status {
                self.newEventsView?.finishUploadPicture()
            } else {
                self.newEventsView?.failRequest(message: message)
            }
        }
    }
}

