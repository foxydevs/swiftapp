//
//  Invitation.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 8/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

class Invitation {
    var id: Int
    var state: Int
    var user_send: Int
    var user_receipt: Int
    var send: User
    
    init(json: JSON) {
        id = json["id"].intValue
        state = json["state"].intValue
        user_send = json["user_send"].intValue
        user_receipt = json["user_receipt"].intValue
        send = User(json: json["send"])
    }
}
