//
//  ProfilePresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 2/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ProfileView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func uploadPicture()
    func setUser(user: User)
    func failRequest(message: String)
}

class ProfilePresenter {
    fileprivate let userService: UserService
    weak fileprivate var profileView: ProfileView?
    
    init(userService: UserService) {
        self.userService = userService
    }
    
    func attachView(_ view: ProfileView) {
        profileView = view
    }
    
    func detachView() {
        profileView = nil
    }
    
    func getUser(id: String) {
        profileView?.startLoading()
        userService.getUser(id: id) { (user, status) in
            self.profileView?.finishLoading()
            if status {
                let userR = User(json: user)
                self.profileView?.setUser(user: userR)
            } else {
                self.profileView?.failRequest(message: "Error fetching User")
            }
        }
    }
    
    func uploadPicture(id: String, photo: UIImage) -> Void{
        profileView?.startLoading()
        self.userService.updateAvatar(id: id, photo: photo) { (user, status, message) in
            self.profileView?.finishLoading()
            if status {
                let preferences = UserDefaults.standard
                let valuePicture = user["picture"].stringValue
                preferences.set(valuePicture, forKey: Constants.keyPicture)
                preferences.synchronize()
                
                Singleton.shared.loadUser()
                self.profileView?.uploadPicture()
            } else {
                self.profileView?.failRequest(message: message)
            }
        }
    }
}
