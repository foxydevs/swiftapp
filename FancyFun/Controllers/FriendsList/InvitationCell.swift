//
//  InvitationCell.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit

class InvitationCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView?
    @IBOutlet weak var name: UILabel?
    @IBOutlet weak var btnAccept: UIButton?
    @IBOutlet weak var btnCancel: UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
