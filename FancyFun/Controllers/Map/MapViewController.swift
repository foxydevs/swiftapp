//
//  MapViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 20/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import MapKit
import AlamofireImage

class MapViewController: UIViewController {

    @IBOutlet weak var map: MKMapView?
    @IBOutlet weak var cardView: UIView?
    @IBOutlet weak var name: UILabel?
    @IBOutlet weak var phone: UILabel?
    @IBOutlet weak var avatar: UIImageView?
    
    var user: User?
    let annotation = MKPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        map?.delegate = self
        map?.showsUserLocation = true
        
        let tapCard = UITapGestureRecognizer(target: self, action: #selector(goToProfile(sender:)))
        tapCard.numberOfTapsRequired = 1
        tapCard.numberOfTouchesRequired = 1
        cardView?.isUserInteractionEnabled = true
        cardView?.addGestureRecognizer(tapCard)
        
        loadLayout()
    }
    
    func loadLayout() -> Void {
        let shadowPath = UIBezierPath(roundedRect: (cardView?.bounds)!, cornerRadius: 2)
        
        cardView?.layer.masksToBounds = false
        cardView?.layer.cornerRadius = 5
        cardView?.layer.shadowColor = UIColor.black.cgColor
        cardView?.layer.shadowOffset = CGSize(width: 0, height: 3)
        cardView?.layer.shadowOpacity = 0.5
        cardView?.layer.shadowPath = shadowPath.cgPath
        
        name?.text = user?.getFullName()
        phone?.text = user?.phone
        avatar?.af_setImage(withURL: (user?.getAvatarURL())!)
        avatar?.layer.cornerRadius = (avatar?.frame.size.height)!/2
        avatar?.layer.masksToBounds = true
        
        annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees((user?.last_latitud)!), longitude: CLLocationDegrees((user?.last_longitud)!))
        map?.addAnnotation(annotation)
        
        let camera = MKMapCamera(lookingAtCenter: annotation.coordinate, fromDistance: 1000, pitch: 0, heading: 0)
        map?.setCamera(camera, animated: true)
    }
    
    @IBAction func goToProfile(sender: UITapGestureRecognizer) -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileView") as! UserProfileViewController
        vc.user = user
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
        }
        
        if let annotationView = annotationView {
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "pinmap")
        }
        
        return annotationView
    }
}
