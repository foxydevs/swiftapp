//
//  SingleEventViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 12/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftyJSON
import AlamofireImage
import MapKit
import Material
import SKPhotoBrowser

class SingleEventViewController: UIViewController {

    @IBOutlet weak var scrollBack: UIScrollView?
    @IBOutlet weak var pictureEvent: UIImageView?
    @IBOutlet weak var avatarUser: UIImageView?
    @IBOutlet weak var userName: UILabel?
    @IBOutlet weak var dateEvent: UILabel?
    @IBOutlet weak var timeEvent: UILabel?
    @IBOutlet weak var eventAssistant: UILabel?
    @IBOutlet weak var eventInterest: UILabel?
    @IBOutlet weak var descriptionEvent: UILabel?
    @IBOutlet weak var mapView: MKMapView?
    @IBOutlet weak var imageAssist: UIImageView?
    @IBOutlet weak var imageInterest: UIImageView?
    @IBOutlet weak var lblUserEvents: UILabel?
    @IBOutlet weak var scrollUserEvents: UIScrollView?
    @IBOutlet weak var btnReport: UIButton?
    
    var event: Event = Event(json: [:])
    var userEvent: UserEvent?
    var pictures = [SKPhoto]()
    var advertisings = [SKPhoto]()
    fileprivate let singleEventPresenter = SingleEventPresenter(eventsService: EventsService())
    let annotation = MKPointAnnotation()
    var haveUserEvent = false
    var userInEvent = [UserEvent]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mapView?.delegate = self
        mapView?.showsUserLocation = true
        mapView?.isUserInteractionEnabled = true
        singleEventPresenter.attachView(self)
        singleEventPresenter.getEvent(id: event.id)
        
        let button = FABButton(image: Icon.place, tintColor: .white)
        button.pulseColor = .white
        button.backgroundColor = Color.indigo.accent4
        button.addTarget(self, action: #selector(showActions(sender:)), for: .touchUpInside)
        view.layout(button).width(ButtonLayout.Fab.diameter).height(ButtonLayout.Fab.diameter).bottomRight(bottom: 10, right: 10)
        lblUserEvents?.text = NSLocalizedString("text_userinevent", comment: "")
        btnReport?.setTitle(NSLocalizedString("text_reportevent", comment: ""), for: .normal)
    }
    
    func loadLayout() {
        scrollBack?.contentSize = CGSize(width: Constants.screenWidth, height: 920)
        
        let numberAssistant = event.assistants.count
        let numberInterest = event.interested.count

        if event.picture != "" {
            pictureEvent?.af_setImage(withURL: event.getPictureURL())
        }
        avatarUser?.af_setImage(withURL: event.user.getAvatarURL())
        userName?.text = event.user.getFullName()
        setformats(date: event.date, time: event.time)
        descriptionEvent?.text = event.description
        eventAssistant?.text = "\(numberAssistant) \(NSLocalizedString("assist_text", comment: ""))"
        eventInterest?.text = "\(numberInterest) \(NSLocalizedString("interest_text", comment: ""))"
        annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(event.latitude), longitude: CLLocationDegrees(event.longitude))
        mapView?.addAnnotation(annotation)
        
        let camera = MKMapCamera(lookingAtCenter: annotation.coordinate, fromDistance: 1000, pitch: 0, heading: 0)
        mapView?.setCamera(camera, animated: true)
        
        let pictureFirst = SKPhoto.photoWithImageURL(event.picture)
        pictureFirst.shouldCachePhotoURLImage = true
        pictures.append(pictureFirst)
        
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(showPictures(sender:)))
        tapImage.numberOfTapsRequired = 1
        tapImage.numberOfTouchesRequired = 1
        pictureEvent?.isUserInteractionEnabled = true
        pictureEvent?.addGestureRecognizer(tapImage)
        
        imageAssist?.image = UIImage(named: "icon_assist")
        for assist: UserEvent in event.assistants {
            if assist.user.id == Int(Singleton.shared.userID) {
                imageAssist?.image = UIImage(named: "icon_assist_fill")
                haveUserEvent = true
                userEvent = assist
            }
        }
        
        imageInterest?.image = UIImage(named: "icon_interest")
        for interest: UserEvent in event.interested {
            if interest.user.id == Int(Singleton.shared.userID) {
                imageInterest?.image = UIImage(named: "icon_interest_fill")
                haveUserEvent = true
                userEvent = interest
            }
        }
        
        if event.user_created == Int(Singleton.shared.userID)! {
            let buttonEdit: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_edit"), style: .plain, target: self, action: #selector(goToEdit(sender:)))
            let buttonDelete: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_trash"), style: .plain, target: self, action: #selector(deleteEvent(sender:)))
            self.navigationItem.rightBarButtonItems = [buttonEdit, buttonDelete]
            self.btnReport?.isHidden = true
        }
        
        if event.advertisings.count > 0 {
            for advertising: Advertising in event.advertisings {
                let pictureAdvertising = SKPhoto.photoWithImageURL(advertising.picture)
                pictureAdvertising.shouldCachePhotoURLImage = true
                advertisings.append(pictureAdvertising)
            }
            
            let browser = SKPhotoBrowser(photos: advertisings)
            browser.initializePageIndex(0)
            self.present(browser, animated: true, completion: nil)
        }
        
        userInEvent.removeAll()
        for userEvent: UserEvent in (event.assistants) {
            userInEvent.append(userEvent)
        }
        
        for userEvent: UserEvent in (event.interested) {
            userInEvent.append(userEvent)
        }
        
        var xFriends = 10;
        var indexFriend = 0
        for userEvent: UserEvent in userInEvent {
            let viewFriend = UIView(frame: CGRect(x: xFriends, y: 0, width: 100, height: 120))
            let avatarFriend = UIImageView(frame: CGRect(x: 15, y: 0, width: 70, height: 70))
            avatarFriend.af_setImage(withURL: userEvent.user.getAvatarURL())
            avatarFriend.layer.cornerRadius = avatarFriend.frame.size.height/2
            avatarFriend.layer.masksToBounds = true
            let nameFriend = UILabel(frame: CGRect(x: 0, y: 70, width: 100, height: 50))
            nameFriend.text = userEvent.user.getFullName()
            nameFriend.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            nameFriend.textAlignment = .center
            nameFriend.numberOfLines = 2
            nameFriend.font = UIFont(name: "Helvetica Neue", size: 11)
            viewFriend.addSubview(avatarFriend)
            viewFriend.addSubview(nameFriend)
            let buttonFriend = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 120))
            buttonFriend.tag = indexFriend
            buttonFriend.addTarget(self, action: #selector(goToUserProfile(sender:)), for: .touchUpInside)
            viewFriend.addSubview(buttonFriend)
            scrollUserEvents?.addSubview(viewFriend)
            xFriends = xFriends + 110
            indexFriend += 1
        }
        
        scrollUserEvents?.contentSize = CGSize(width: xFriends + 10, height: 120)
    }
    
    func goToUserProfile(sender: UIButton) -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileView") as! UserProfileViewController
        vc.user = userInEvent[sender.tag].user
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToEdit(sender: UIBarButtonItem) -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewEventView") as! NewEventViewController
        vc.title = NSLocalizedString("updateevent_text", comment: "")
        vc.eventUpdate = self.event
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteEvent(sender: UIBarButtonItem) -> Void {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("deleteevent_text", comment: ""), preferredStyle: .alert)
        
        let optionDelete = UIAlertAction(title: NSLocalizedString("delete_text", comment: ""), style: .destructive) { (UIAlertAction) in
            
            self.singleEventPresenter.deleteEvent(id: self.event.id)
        }
        
        let optionCancel = UIAlertAction(title: NSLocalizedString("cancel_text", comment: ""), style: .cancel) { (UIAlertAction) in}
        
        alert.addAction(optionDelete)
        alert.addAction(optionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func setformats(date: String, time: String) -> Void {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let dateC = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat = "d MMMM yyyy"
        let newDate = dateFormatter.string(from: dateC!)
        
        
        dateEvent?.text = newDate
        timeEvent?.text = time
    }
    
    @IBAction func showActions(sender: FABButton) -> Void {
        let alert = UIAlertController(title: NSLocalizedString("navigate_text", comment: ""), message: NSLocalizedString("navigateapp_text", comment: ""), preferredStyle: .actionSheet)
        
        if (UIApplication.shared.canOpenURL(URL(string:"https://waze.com/lu")!)){
            let optionWaze = UIAlertAction(title: "Waze", style: .default) { (UIAlertAction) in
                let wazeString = "https://waze.com/ul?ll=\(self.event.latitude),\(self.event.longitude))&navigate=yes"
                UIApplication.shared.open(URL(string: wazeString)!, options: [:], completionHandler: nil)
            }
            alert.addAction(optionWaze)
        }
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)){
            let optionGoogleMaps = UIAlertAction(title: "Google Maps", style: .default) { (UIAlertAction) in
                let googleString = "comgooglemaps://?center=\(self.event.latitude),\(self.event.longitude)&zoom=14&views=traffic&q=\(self.event.latitude),\(self.event.longitude)"
                UIApplication.shared.open(URL(string: googleString)!, options: [:], completionHandler: nil)
            }
            alert.addAction(optionGoogleMaps)
        }
        
        let optionMapsApple = UIAlertAction(title: "Maps Apple", style: .default) { (UIAlertAction) in
            let appleString = "http://maps.apple.com/?ll=\(self.event.latitude),\(self.event.longitude)"
            UIApplication.shared.open(URL(string: appleString)!, options: [:], completionHandler: nil)
        }
        
        let optionCancel = UIAlertAction(title: NSLocalizedString("cancel_text", comment: ""), style: .cancel) { (UIAlertAction) in
            
        }
        
        alert.addAction(optionMapsApple)
        alert.addAction(optionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func showPictures(sender: UITapGestureRecognizer) -> Void {
        let browser = SKPhotoBrowser(photos: pictures)
        browser.initializePageIndex(0)
        self.present(browser, animated: true, completion: nil)
    }
    
    @IBAction func userEventAction(sender: UIButton) -> Void {
        if sender.tag == 1 {
            if imageAssist?.image == UIImage(named: "icon_assist_fill") {
                singleEventPresenter.deleteUserEvent(id: (userEvent?.id)!)
            } else {
                if haveUserEvent {
                    singleEventPresenter.updateUserEvent(id: (userEvent?.id)!, parameters: ["state": 1])
                } else {
                    singleEventPresenter.createUserEvent(parameters: ["event": event.id, "user": Singleton.shared.userID, "state": 1])
                }
            }
        } else {
            if imageAssist?.image == UIImage(named: "icon_interest_fill") {
                singleEventPresenter.deleteUserEvent(id: (userEvent?.id)!)
            } else {
                if haveUserEvent {
                    singleEventPresenter.updateUserEvent(id: (userEvent?.id)!, parameters: ["state": 0])
                } else {
                    singleEventPresenter.createUserEvent(parameters: ["event": event.id, "user": Singleton.shared.userID, "state": 0])
                }
            }
        }
    }
    
    @IBAction func reportEvent(sender: UIButton) -> Void {
        singleEventPresenter.reportEvent(id: event.id)
    }
}

extension SingleEventViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "pinmap")
        }
        
        return annotationView
    }
}

extension SingleEventViewController: SingleVentView {
    func startLoading() {
        KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
    }
    
    func finishLoading() {
        KRProgressHUD.dismiss()
    }
    
    func reloadEvent() {
        singleEventPresenter.getEvent(id: event.id)
    }
    
    func setEvent(event: Event) {
        self.event = event
        self.loadLayout()
    }
    
    
    func deletedEvent() {
        Singleton.shared.reloadEvent = true
        self.navigationController?.popViewController(animated: true)
    }
    
    func failRequest() {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_events", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
