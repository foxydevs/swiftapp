//
//  UserProfilePresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 8/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol UserProfileView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func reloadUser()
    func setUser(user: JSON)
    func failRequest(message: String)
}

class UserProfilePresenter {
    fileprivate let userService: UserService
    fileprivate let friendsService: FriendsService
    weak fileprivate var userProfileView: UserProfileView?
    
    init(userService: UserService, friendsService: FriendsService) {
        self.userService = userService
        self.friendsService = friendsService
    }
    
    func attachView(_ view: UserProfileView) {
        userProfileView = view
    }
    
    func detachView() {
        userProfileView = nil
    }
    
    func getUser(id: Int) -> Void {
        userProfileView?.startLoading()
        userService.getUserProfile(friend: id, user: Singleton.shared.userID) { (user, status) in
            self.userProfileView?.finishLoading()
            if status {
                self.userProfileView?.setUser(user: user)
            } else {
                self.userProfileView?.failRequest(message: "Error Fetching User")
            }
        }
    }
    
    func sendInvitation(parameters: JSON) {
        userProfileView?.startLoading()
        friendsService.sendInvitation(parameters: parameters) { (invitation, status, message) in
            self.userProfileView?.finishLoading()
            if status {
                self.userProfileView?.reloadUser()
            } else {
                self.userProfileView?.failRequest(message: message)
            }
        }
    }
    
    func deleteInvitation(user: String, friend: Int) {
        userProfileView?.startLoading()
        friendsService.removeFriend(user: user, friend: friend) { (invitation, status, message) in
            self.userProfileView?.finishLoading()
            if status {
                self.userProfileView?.reloadUser()
            } else {
                self.userProfileView?.failRequest(message: message)
            }
        }
    }
}

