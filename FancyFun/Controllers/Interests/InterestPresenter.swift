//
//  InterestPresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 3/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol InterestView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setInterests(interest: JSON)
    func reloadInterests()
    func failRequest(message: String)
}

class InterestPresenter {
    fileprivate let interestService: InterestService
    weak fileprivate var interestView: InterestView?
    
    init(interestService: InterestService) {
        self.interestService = interestService
    }
    
    func attachView(_ view: InterestView) {
        interestView = view
    }
    
    func detachView() {
        interestView = nil
    }
    
    func getInterest(id: String) {
        interestView?.startLoading()
        interestService.getInterest(id: id) { (interests, status) in
            self.interestView?.finishLoading()
            if status {
                self.interestView?.setInterests(interest: interests)
            } else {
                self.interestView?.failRequest(message: "Fail Request")
            }
        }
    }
    
    func createInterest(parameters: JSON) {
        interestView?.startLoading()
        interestService.createInterest(parameters: parameters) { (interest, status, message) in
            self.interestView?.finishLoading()
            if status {
                self.interestView?.reloadInterests()
            } else {
                self.interestView?.failRequest(message: "Fail Request")
            }
        }
    }
    
    func deleteInterest(id: Int) {
        interestView?.startLoading()
        interestService.deleteInterest(id: id) { (interest, status, message) in
            self.interestView?.finishLoading()
            if status {
                self.interestView?.reloadInterests()
            } else {
                self.interestView?.failRequest(message: "Fail Request")
            }
        }
    }
}
