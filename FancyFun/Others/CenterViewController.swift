//
//  CenterViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 8/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import SideMenuController
import Material
import CoreLocation
import SwiftyJSON

class CenterViewController: SideMenuController {
    
    let locationManager: CLLocationManager = CLLocationManager()
    var currentLocation: CLLocation?
    let userService = UserService()
    var timeToUpdate = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let vcE = self.storyboard?.instantiateViewController(withIdentifier: "EventsView") as! EventsViewController
        vcE.title = NSLocalizedString("menu_near", comment: "")
        vcE.pageTabBarItem.title = NSLocalizedString("menu_near", comment: "")
        vcE.pageTabBarItem.titleColor = Constants.tabBarText
        vcE.friendEvents = false
        let vcF = self.storyboard?.instantiateViewController(withIdentifier: "EventsView") as! EventsViewController
        vcF.title = NSLocalizedString("menu_friends", comment: "")
        vcF.pageTabBarItem.title = NSLocalizedString("menu_friends", comment: "")
        vcF.pageTabBarItem.titleColor = Constants.tabBarText
        vcF.friendEvents = true
        let vcMenu = self.storyboard?.instantiateViewController(withIdentifier: "MenuView") as! MenuViewController
        
        sideMenuController?.embed(centerViewController: UINavigationController(rootViewController: AppPageTabBarController(viewControllers: [vcE, vcF], selectedIndex: 0)))
        sideMenuController?.embed(sideViewController: vcMenu)
        
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
        
        getFriends()
    }
    
    func updateUserLocation() -> Void {
        if Singleton.shared.userID != "" {
            let parameterhs: JSON = ["last_latitud": Singleton.shared.currentLocation?.coordinate.latitude ?? 0.0, "last_longitud": Singleton.shared.currentLocation?.coordinate.longitude ?? 0.0]
            userService.updateUser(id: Singleton.shared.userID, parameters: parameterhs) { (user, status, message) in
                print("update user location")
            }
        }
    }
    
    func getFriends() -> Void {
        let friendService = FriendsService()
        friendService.getFriends(id: Singleton.shared.userID) { (friends, status) in
            if status {
                print("friends loaded")
                Singleton.shared.friends.removeAll()
                for user: JSON in friends.array! {
                    Singleton.shared.friends.append(User(json: user))
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CenterViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        Singleton.shared.currentLocation = locations[0]
        if timeToUpdate == 10 {
            updateUserLocation()
            timeToUpdate = 0
        }
        timeToUpdate += 1
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

