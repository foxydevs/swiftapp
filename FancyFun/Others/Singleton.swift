//
//  Singleton.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 31/07/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

final class Singleton {
    private init() {}
    
    static let shared = Singleton()
    
    var userID: String = "0"
    var firstName: String = ""
    var lastName: String = ""
    var fullName: String = ""
    var pictureURL: String = ""
    var reloadEvent: Bool = false
    var distance: Int = 5
    var distanceKey: Int = 0
    var currentLocation: CLLocation?
    var friends = [User]()
    
    func loadUser() -> Void {
        let preferences = UserDefaults.standard
        userID = preferences.value(forKey: Constants.keyId) as! String
        firstName = preferences.value(forKey: Constants.keyFirstName) as! String
        lastName = preferences.value(forKey: Constants.keyLastName) as! String
        fullName = "\(firstName) \(lastName)"
        pictureURL = preferences.value(forKey: Constants.keyPicture) as! String
        distanceKey = preferences.value(forKey: Constants.keyDistance) as! Int
        switch distanceKey {
        case 0:
            distance = 5
            break
        case 1:
            distance = 10
            break
        case 2:
            distance = 15
            break
        default:
            distance = 5
            break
        }
        
    }
    
    func isFriend(user: User) -> Bool {
        for userIterator: User in friends {
            if user.id == userIterator.id {
                return true
            }
        }
        return false
    }
    
    func fixOrientation(imageChange: UIImage) -> UIImage
    {
        
        if imageChange.imageOrientation == UIImageOrientation.up {
            return imageChange
        }
        
        var transform = CGAffineTransform.identity
        
        switch imageChange.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: imageChange.size.width, y: imageChange.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi));
            
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: imageChange.size.width, y: 0);
            transform = transform.rotated(by: CGFloat(Double.pi/2));
            
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: imageChange.size.height);
            transform = transform.rotated(by: CGFloat(-Double.pi/2));
            
        case .up, .upMirrored:
            break
        }
        
        
        switch imageChange.imageOrientation {
            
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: imageChange.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: imageChange.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1);
            
        default:
            break;
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx = CGContext(
            data: nil,
            width: Int(imageChange.size.width),
            height: Int(imageChange.size.height),
            bitsPerComponent: imageChange.cgImage!.bitsPerComponent,
            bytesPerRow: 0,
            space: imageChange.cgImage!.colorSpace!,
            bitmapInfo: UInt32(imageChange.cgImage!.bitmapInfo.rawValue)
        )
        
        
        
        ctx!.concatenate(transform);
        
        switch imageChange.imageOrientation {
            
        case .left, .leftMirrored, .right, .rightMirrored:
            // Grr...
            ctx?.draw(imageChange.cgImage!, in: CGRect(x:0 ,y: 0 ,width: imageChange.size.height ,height:imageChange.size.width))
            
        default:
            ctx?.draw(imageChange.cgImage!, in: CGRect(x:0 ,y: 0 ,width: imageChange.size.width ,height:imageChange.size.height))
            break;
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg = ctx!.makeImage()
        
        let img = UIImage(cgImage: cgimg!)
        
        return img;
        
    }
}
