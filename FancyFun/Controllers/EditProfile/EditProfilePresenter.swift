//
//  EditProfilePresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol EditProfileView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func updatedUser(user: JSON)
    func failRequest(message: String)
}

class EditProfilePresenter {
    fileprivate let userService: UserService
    weak fileprivate var editProfileView: EditProfileView?
    
    init(userService: UserService) {
        self.userService = userService
    }
    
    func attachView(_ view: EditProfileView) {
        editProfileView = view
    }
    
    func detachView() {
        editProfileView = nil
    }
    
    func updateProfile(parameters: JSON) -> Void {
        editProfileView?.startLoading()
        userService.updateUser(id: Singleton.shared.userID, parameters: parameters) { (user, status, message) in
            self.editProfileView?.finishLoading()
            if status {
                let preferences = UserDefaults.standard
                let valueId = user["id"].stringValue
                let valueFirstName = user["firstname"].stringValue
                let valueLastName = user["lastname"].stringValue
                let valuePicture = user["picture"].stringValue
                preferences.set(valueId, forKey: Constants.keyId)
                preferences.set(valueFirstName, forKey: Constants.keyFirstName)
                preferences.set(valueLastName, forKey: Constants.keyLastName)
                preferences.set(valuePicture, forKey: Constants.keyPicture)
                preferences.synchronize()
                
                Singleton.shared.loadUser()
                self.editProfileView?.updatedUser(user: user)
            } else {
                self.editProfileView?.failRequest(message: message)
            }
        }
    }
}
