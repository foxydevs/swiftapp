//
//  FriendsService.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class FriendsService {
    func getNoFriends(id: String,_ callback:@escaping (JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/users/\(id)/nofriends"
        Alamofire.request(urlString)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func getNearPeople(parameters: JSON, callback:@escaping(JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/near/people"
        Alamofire.request(urlString, method: .get, parameters: parameters.dictionaryObject)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, "")
                } else {
                    status = false
                    callback(json, status, "")
                }
        }
    }
    
    func getNotFriendsNear(parameters: JSON, callback:@escaping(JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/near/friends"
        Alamofire.request(urlString, method: .get, parameters: parameters.dictionaryObject)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, "")
                } else {
                    status = false
                    callback(json, status, "")
                }
        }
    }
    
    func getFriends(id: String,_ callback:@escaping (JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/users/\(id)/friends"
        Alamofire.request(urlString)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func sendInvitation(parameters: JSON,_ callback:@escaping(JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/friends"
        Alamofire.request(urlString, method: .post, parameters: parameters.dictionaryObject, encoding: JSONEncoding.default)
            .response { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, "")
                } else if response.response?.statusCode == 409 {
                    status = false
                    callback(json, status, NSLocalizedString("error_invitation", comment: ""))
                } else {
                    status = false
                    callback(json, status, NSLocalizedString("error_server", comment: ""))
                }
        }
    }
    
    func acceptInvitation(id: Int, parameters: JSON,_ callback:@escaping(JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/friends/\(id)"
        Alamofire.request(urlString, method: .put, parameters: parameters.dictionaryObject, encoding: JSONEncoding.default)
            .response { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, "")
                } else {
                    status = false
                    callback(json, status, NSLocalizedString("error_server", comment: ""))
                }
        }
    }
    
    func cancelInvitation(id: Int,_ callback:@escaping(JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/friends/\(id)"
        Alamofire.request(urlString, method: .delete, parameters: [:], encoding: JSONEncoding.default)
            .response { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, "")
                } else {
                    status = false
                    callback(json, status, NSLocalizedString("error_server", comment: ""))
                }
        }
    }
    
    func removeFriend(user: String, friend: Int,_ callback:@escaping(JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/friends/\(user)/\(friend)"
        Alamofire.request(urlString, method: .delete, parameters: [:], encoding: JSONEncoding.default)
            .response { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, "")
                } else {
                    status = false
                    callback(json, status, NSLocalizedString("error_server", comment: ""))
                }
        }
    }
}
