//
//  SignInViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftyJSON
import OneSignal
import FBSDKCoreKit
import FBSDKLoginKit

class SignInViewController: UIViewController {

    @IBOutlet weak var btnSignIn: UIButton?
    @IBOutlet weak var btnFacebook: UIButton?
    @IBOutlet weak var txtUsername: UITextField?
    @IBOutlet weak var txtPassword: UITextField?
    
    fileprivate let signInPresenter = SignInPresenter(userService: UserService())
    fileprivate var oneSignalId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("HomeSignIn", comment: "")
        self.initializeLayout()
        
        signInPresenter.attachView(self)
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        if let id = status.subscriptionStatus.userId {
            oneSignalId = id
        }
    }
    
    func initializeLayout() -> Void {
        self.setupButton(sender: btnSignIn!)
        self.setupButton(sender: btnFacebook!)
        self.btnSignIn?.setTitle(NSLocalizedString("HomeSignIn", comment: ""), for: .normal)
        self.btnFacebook?.setTitle(NSLocalizedString("facebook_text", comment: ""), for: .normal)
        self.txtUsername?.text = ""
        self.txtPassword?.text = ""
        self.txtUsername?.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("username_text", comment: ""),
                                                                     attributes: [NSForegroundColorAttributeName: Constants.placeHolderText])
        self.txtPassword?.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("password_text", comment: ""),
                                                                     attributes: [NSForegroundColorAttributeName: Constants.placeHolderText])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupButton(sender: UIButton) -> Void {
        sender.layer.cornerRadius = 8
        sender.layer.borderColor = UIColor.white.cgColor
        sender.layer.borderWidth = 1
    }
    
    @IBAction func actionSignIn(_ sender: UIButton) -> Void {
        if ((self.txtUsername?.text?.characters.count)! > 0 && (self.txtPassword?.text?.characters.count)! > 0) {
            let parameters: JSON = ["username": self.txtUsername?.text ?? "", "password": self.txtPassword?.text ?? "", "one_signal_id": oneSignalId]
            signInPresenter.doLogin(parameters: parameters)
        } else {
            let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_fields", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func loginWithFacebook(_sender: UIButton) -> Void {
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil) {
                let fbLoginResult: FBSDKLoginManagerLoginResult = (result)!
                if (fbLoginResult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                }
            } else {
                print(error.debugDescription)
            }
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    let dataJSON = JSON(result ?? "")
                    let parameters: JSON = ["username": dataJSON["id"].stringValue, "facebook_id": dataJSON["id"].stringValue, "firstname": dataJSON["first_name"].stringValue, "lastname": dataJSON["last_name"].stringValue, "email": dataJSON["email"].stringValue, "one_signal_id": self.oneSignalId, "picture": dataJSON["picture"]["data"]["url"].stringValue]
                    self.signInPresenter.doLoginFacebook(parameters: parameters)
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SignInViewController: SignInView {
    func startLoading() {
        KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
    }
    
    func finishLoading() {
        KRProgressHUD.dismiss()
    }
    
    func successLogin() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CenterView") as! CenterViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func failLogin() {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_login", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
