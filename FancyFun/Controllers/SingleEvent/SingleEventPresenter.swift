//
//  SingleEventPresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 12/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SingleVentView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func reloadEvent()
    func setEvent(event: Event)
    func deletedEvent()
    func failRequest()
}

class SingleEventPresenter{
    fileprivate let eventsService: EventsService
    weak fileprivate var singleEventView: SingleVentView?
    
    init(eventsService: EventsService) {
        self.eventsService = eventsService
    }
    
    func attachView(_ view: SingleVentView) {
        singleEventView = view
    }
    
    func detachView() {
        singleEventView = nil
    }
    
    func getEvent(id: Int) -> Void {
        singleEventView?.startLoading()
        eventsService.getEvent(id: id) { (event, status) in
            self.singleEventView?.finishLoading()
            if status {
                self.singleEventView?.setEvent(event: Event(json: event))
            } else {
                self.singleEventView?.failRequest()
            }
        }
    }
    
    func deleteEvent(id: Int) -> Void {
        singleEventView?.startLoading()
        eventsService.deleteEvent(id: id) { (event, status, message) in
            self.singleEventView?.finishLoading()
            if status {
                self.singleEventView?.deletedEvent()
            } else {
                self.singleEventView?.failRequest()
            }
        }
    }
    
    func createUserEvent(parameters: JSON) -> Void {
        singleEventView?.startLoading()
        eventsService.createUserEvent(parameters: parameters) { (userevent, status, message) in
            self.singleEventView?.finishLoading()
            if status {
                self.singleEventView?.reloadEvent()
            } else {
                self.singleEventView?.failRequest()
            }
        }
    }
    
    func updateUserEvent(id: Int, parameters: JSON) -> Void {
        singleEventView?.startLoading()
        eventsService.updateUserEvent(id: id, parameters: parameters) { (userevent, status, message) in
            self.singleEventView?.finishLoading()
            if status {
                self.singleEventView?.reloadEvent()
            } else {
                self.singleEventView?.failRequest()
            }
        }
    }
    
    func deleteUserEvent(id: Int) -> Void {
        singleEventView?.startLoading()
        eventsService.deleteUserEvent(id: id) { (userevent, status, message) in
            self.singleEventView?.finishLoading()
            if status {
                self.singleEventView?.reloadEvent()
            } else {
                self.singleEventView?.failRequest()
            }
        }
    }
    
    func reportEvent(id: Int) -> Void {
        singleEventView?.startLoading()
        eventsService.reportEvent(id: id) { (event, status) in
            self.singleEventView?.finishLoading()
            if status {
                self.singleEventView?.reloadEvent()
            } else {
                self.singleEventView?.failRequest()
            }
        }
    }
}
