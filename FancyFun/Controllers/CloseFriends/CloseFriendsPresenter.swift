//
//  CloseFriendsPresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 15/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol CloseFriendsView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func reloadFriends()
    func setFriends(friends: JSON)
    func failRequest(message: String)
}

class CloseFriendsPresenter {
    fileprivate let friendsService: FriendsService
    fileprivate let userService: UserService
    weak fileprivate var closeFriendsView: CloseFriendsView?
    
    init(friendsService: FriendsService, userService: UserService) {
        self.friendsService = friendsService
        self.userService = userService
    }
    
    func attachView(_ view: CloseFriendsView) {
        closeFriendsView = view
    }
    
    func detachView() {
        closeFriendsView = nil
    }
    
    func getNoFriends(id: String) {
        closeFriendsView?.startLoading()
        friendsService.getNoFriends(id: id) { (nofriends, status) in
            self.closeFriendsView?.finishLoading()
            if status {
                self.closeFriendsView?.setFriends(friends: nofriends)
            } else {
                self.closeFriendsView?.failRequest(message: "")
            }
        }
    }
    
    func getNearPeople(parameters: JSON) -> Void {
        closeFriendsView?.startLoading()
        friendsService.getNearPeople(parameters: parameters) { (nofriends, status, message) in
            self.closeFriendsView?.finishLoading()
            if status {
                self.closeFriendsView?.setFriends(friends: nofriends)
            } else {
                self.closeFriendsView?.failRequest(message: "")
            }
        }
    }
    
    func getNotFriendsNear(parameters: JSON) -> Void {
        closeFriendsView?.startLoading()
        friendsService.getNotFriendsNear(parameters: parameters) { (nofriends, status, message) in
            self.closeFriendsView?.finishLoading()
            if status {
                self.closeFriendsView?.setFriends(friends: nofriends)
            } else {
                self.closeFriendsView?.failRequest(message: "")
            }
        }
    }
    
    func sendInvitation(parameters: JSON) {
        closeFriendsView?.startLoading()
        friendsService.sendInvitation(parameters: parameters) { (invitation, status, message) in
            self.closeFriendsView?.finishLoading()
            if status {
                self.closeFriendsView?.reloadFriends()
            } else {
                self.closeFriendsView?.failRequest(message: message)
            }
        }
    }
    
    func updateUser(parameters: JSON) -> Void {
        userService.updateUser(id: Singleton.shared.userID, parameters: parameters) { (user, status, message) in
            print("UPDATED LOCATION")
        }
    }
}
