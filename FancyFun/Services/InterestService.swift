//
//  InterestService.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 3/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class InterestService {
    func getInterest(id: String,_ callback:@escaping (JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/interest/filter/\(id)"
        Alamofire.request(urlString)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func createInterest(parameters: JSON,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/interest"
        Alamofire.request(urlString, method: .post, parameters: parameters.dictionaryObject)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, response.description)
                } else {
                    status = false
                    callback(json, status, json["message"].stringValue)
                }
        }
    }
    
    func deleteInterest(id: Int,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/interest/\(id)"
        Alamofire.request(urlString, method: .delete, parameters: [:])
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, response.description)
                } else {
                    status = false
                    callback(json, status, json["message"].stringValue)
                }
        }
    }
}
