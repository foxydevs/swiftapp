//
//  EditProfileViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 4/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftyJSON

class EditProfileViewController: UIViewController {

    @IBOutlet weak var scrollBack: UIScrollView?
    @IBOutlet weak var viewFirstName: UIView?
    @IBOutlet weak var viewLastName: UIView?
    @IBOutlet weak var viewWork: UIView?
    @IBOutlet weak var viewDescription: UIView?
    @IBOutlet weak var viewPhone: UIView?
    @IBOutlet weak var viewBirthday: UIView?
    @IBOutlet weak var viewDistance: UIView?
    
    @IBOutlet weak var titleName: UILabel?
    @IBOutlet weak var titleLastName: UILabel?
    @IBOutlet weak var titleWork: UILabel?
    @IBOutlet weak var titleDescription: UILabel?
    @IBOutlet weak var titlePhone: UILabel?
    @IBOutlet weak var titleBirthday: UILabel?
    @IBOutlet weak var titleDistance: UILabel?
    
    @IBOutlet weak var inputFirstname: UITextField?
    @IBOutlet weak var inputLastname: UITextField?
    @IBOutlet weak var inputWork: UITextField?
    @IBOutlet weak var inputDescription: UITextField?
    @IBOutlet weak var inputPhone: UITextField?
    @IBOutlet weak var inputBirthday: DateFieldTextField?
    
    @IBOutlet weak var segmentedDistance: UISegmentedControl?
    
    fileprivate let editProfilePresenter = EditProfilePresenter(userService: UserService())
    var userUpdate: User = User(json: [:])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("text_editprofile", comment: "")
        loadLayout()
        editProfilePresenter.attachView(self)
        loadUserInfo()
    }
    
    func loadLayout() {
        let buttonEdit: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_check"), style: .plain, target: self, action: #selector(updateUser(sender:)))
        self.navigationItem.rightBarButtonItem = buttonEdit
        
        self.scrollBack?.contentSize = CGSize(width: Constants.screenWidth, height: 450)
        setShadow(view: viewFirstName!)
        setShadow(view: viewLastName!)
        setShadow(view: viewWork!)
        setShadow(view: viewDescription!)
        setShadow(view: viewPhone!)
        setShadow(view: viewBirthday!)
        setShadow(view: viewDistance!)
        
        titleName?.text = NSLocalizedString("text_title_firstname", comment: "")
        titleLastName?.text = NSLocalizedString("text_title_lastname", comment: "")
        titleWork?.text = NSLocalizedString("text_title_work", comment: "")
        titleDescription?.text = NSLocalizedString("text_title_description", comment: "")
        titlePhone?.text = NSLocalizedString("text_title_phone", comment: "")
        titleBirthday?.text = NSLocalizedString("text_title_birthday", comment: "")
        titleDistance?.text = NSLocalizedString("text_title_distance", comment: "")
        
        inputFirstname?.placeholder = NSLocalizedString("text_title_firstname", comment: "")
        inputLastname?.placeholder = NSLocalizedString("text_title_lastname", comment: "")
        inputWork?.placeholder = NSLocalizedString("text_title_work", comment: "")
        inputDescription?.placeholder = NSLocalizedString("text_title_description", comment: "")
        inputPhone?.placeholder = NSLocalizedString("text_title_phone", comment: "")
        inputBirthday?.placeholder = NSLocalizedString("text_title_birthday", comment: "")
        
        inputBirthday?.dateDelegate = self
        inputBirthday?.datetype = SCDatePicker.SCDatePickerType.date
    }
    
    func loadUserInfo() -> Void {
        inputFirstname?.text = self.userUpdate.firstname
        inputLastname?.text = self.userUpdate.lastname
        inputWork?.text = self.userUpdate.work
        inputDescription?.text = self.userUpdate.userDescription
        inputPhone?.text = self.userUpdate.phone
        inputBirthday?.text = self.userUpdate.birthday
        segmentedDistance?.selectedSegmentIndex = Singleton.shared.distanceKey
    }
    
    @IBAction func updateDistance(segmented: UISegmentedControl) -> Void {
        let preferences = UserDefaults.standard
        preferences.set(segmentedDistance?.selectedSegmentIndex, forKey: Constants.keyDistance)
        preferences.synchronize()
    }
    
    @IBAction func updateUser(sender: UIBarButtonItem) -> Void {
        let firstname = inputFirstname?.text  ?? ""
        let lastname = inputLastname?.text  ?? ""
        let work = inputWork?.text  ?? ""
        let description = inputDescription?.text  ?? ""
        let phone = inputPhone?.text  ?? ""
        let birthday = inputBirthday?.text  ?? ""
        
        let parameters: JSON = ["firstname": firstname, "lastname": lastname, "work": work, "description": description, "phone": phone, "birthday": birthday]
        editProfilePresenter.updateProfile(parameters: parameters)
    }
    
    func setShadow(view: UIView) -> Void {
        let shadowPathUser = UIBezierPath(roundedRect: view.bounds, cornerRadius: 2)
        
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowPath = shadowPathUser.cgPath
    }
}

extension EditProfileViewController: SCDateFieldDelegate {
    func dateTextHasChanged(dateString: String, date: NSDate) {
        print(dateString)
    }
}

extension EditProfileViewController: EditProfileView {
    func startLoading() {
        KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
    }
    
    func finishLoading() {
        KRProgressHUD.dismiss()
    }
    
    func updatedUser(user: JSON) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func failRequest(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_user", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
