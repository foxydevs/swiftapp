//
//  EventsService.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 8/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class EventsService {
    func getEvents(parameters: JSON,_ callback:@escaping (JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/events"
        Alamofire.request(urlString, method: .get, parameters: [:])
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func getNearEvents(parameters: JSON,_ callback:@escaping (JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/near/events"
        Alamofire.request(urlString, method: .get, parameters: parameters.dictionaryObject)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func getEventsFriends(parameters: JSON,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/events/filter/friends"
        Alamofire.request(urlString, method: .get, parameters: parameters.dictionaryObject)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, response.description)
                } else {
                    status = false
                    print(response.description)
                    callback(json, status, response.description)
                }
        }
    }
    
    func createUserEvent(parameters: JSON,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/userevents"
        Alamofire.request(urlString, method: .post, parameters: parameters.dictionaryObject)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                print(json)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, response.description)
                } else {
                    status = false
                    print(response.description)
                    callback(json, status, response.description)
                }
        }
    }
    
    func updateUserEvent(id:Int, parameters: JSON,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/userevents/\(id)"
        Alamofire.request(urlString, method: .put, parameters: parameters.dictionaryObject)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, response.description)
                } else {
                    status = false
                    print(response.description)
                    callback(json, status, response.description)
                }
        }
    }
    
    func deleteUserEvent(id:Int,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/userevents/\(id)"
        Alamofire.request(urlString, method: .delete, parameters: [:])
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, response.description)
                } else {
                    status = false
                    print(response.description)
                    callback(json, status, response.description)
                }
        }
    }


    
    func getEvent(id: Int,_ callback:@escaping (JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "/events/\(id)"
        Alamofire.request(urlString)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }
    
    func createEvent(parameters: JSON,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/events"
        Alamofire.request(urlString, method: .post, parameters: parameters.dictionaryObject)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, response.description)
                } else {
                    status = false
                    callback(json, status, json["message"].stringValue)
                }
        }
    }
    
    func updateEvent(id:Int, parameters: JSON,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/events/\(id)"
        Alamofire.request(urlString, method: .put, parameters: parameters.dictionaryObject)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, response.description)
                } else {
                    status = false
                    callback(json, status, json["message"].stringValue)
                }
        }
    }
    
    func deleteEvent(id:Int,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/events/\(id)"
        Alamofire.request(urlString, method: .delete, parameters: [:])
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true;
                if response.response?.statusCode == 200 {
                    callback(json, status, response.description)
                } else {
                    status = false
                    callback(json, status, json["message"].stringValue)
                }
        }
    }
    
    func insertPictureEvent(id: Int, photo: UIImage,_ callback:@escaping (JSON, Bool, String) -> Void) {
        let urlString: String = Constants.urlBase + "/upload/event/\(id)"
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(UIImageJPEGRepresentation(Singleton.shared.fixOrientation(imageChange: photo), 0.5)!, withName: "avatar", fileName: "jpeg", mimeType: "image/jpeg")
        }, to: urlString) { (encodingResult) in
            switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON{ response in
                        let json = JSON(data: response.data!)
                        callback(json, true, response.description)
                    }
                case .failure(let encodingError):
                    print("FAILURE")
                    print(encodingError)
                    callback([:], false, encodingError.localizedDescription)
            }
        }
    }
    
    func reportEvent(id: Int,_ callback:@escaping (JSON, Bool) -> Void) {
        let urlString: String = Constants.urlBase + "report/event/\(id)"
        Alamofire.request(urlString)
            .responseJSON { (response) in
                let json = JSON(data: response.data!)
                var status = true
                if response.response?.statusCode == 200 {
                    callback(json, status)
                } else {
                    status = false
                    callback(json, status)
                }
        }
    }

}
