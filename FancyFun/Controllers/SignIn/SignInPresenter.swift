//
//  SignInPresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SignInView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func successLogin()
    func failLogin()
}

class SignInPresenter {
    fileprivate let userService: UserService
    weak fileprivate var signInView: SignInView?
    
    init(userService: UserService) {
        self.userService = userService
    }
    
    func attachView(_ view: SignInView) {
        signInView = view
    }
    
    func detachView() {
        signInView = nil
    }
    
    func doLogin(parameters: JSON) {
        signInView?.startLoading()
        userService.doLogin(parameters: parameters) { (json, status) in
            self.signInView?.finishLoading()
            if status {
                let preferences = UserDefaults.standard
                let valueId = json["id"].stringValue
                let valueFirstName = json["firstname"].stringValue
                let valueLastName = json["lastname"].stringValue
                let valuePicture = json["picture"].stringValue
                preferences.set(valueId, forKey: Constants.keyId)
                preferences.set(valueFirstName, forKey: Constants.keyFirstName)
                preferences.set(valueLastName, forKey: Constants.keyLastName)
                preferences.set(valuePicture, forKey: Constants.keyPicture)
                preferences.set(0, forKey: Constants.keyDistance)
                preferences.synchronize()
                
                Singleton.shared.loadUser()
                
                self.signInView?.successLogin()
            } else {
                self.signInView?.failLogin()
            }
        }
    }
    
    func doLoginFacebook(parameters: JSON) -> Void {
        signInView?.startLoading()
        userService.loginFacebook(parameters: parameters) { (user, status) in
            self.signInView?.finishLoading()
            if status {
                let preferences = UserDefaults.standard
                let valueId = user["id"].stringValue
                let valueFirstName = user["firstname"].stringValue
                let valueLastName = user["lastname"].stringValue
                let valuePicture = user["picture"].stringValue
                preferences.set(valueId, forKey: Constants.keyId)
                preferences.set(valueFirstName, forKey: Constants.keyFirstName)
                preferences.set(valueLastName, forKey: Constants.keyLastName)
                preferences.set(valuePicture, forKey: Constants.keyPicture)
                preferences.set(0, forKey: Constants.keyDistance)
                preferences.synchronize()
                
                Singleton.shared.loadUser()
                
                self.signInView?.successLogin()
            } else {
                self.signInView?.failLogin()
            }
        }
    }
}
