//
//  InvitationsViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftyJSON
import AlamofireImage

class InvitationsViewController: UIViewController {

    @IBOutlet weak var tableInvitations: UITableView?
    @IBOutlet weak var lblNoInvitatios: UILabel?
    let refreshControl = UIRefreshControl()
    
    var send: [Invitation] = []
    var receipt: [Invitation] = []
    fileprivate let invitationsPresenter = InvitationsPresenter(userService: UserService(), invitationService: FriendsService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableInvitations?.delegate = self
        tableInvitations?.dataSource = self
        tableInvitations?.tableFooterView = UIView(frame: .zero)
        setupTableView()
        invitationsPresenter.attachView(self)
        invitationsPresenter.getInvitation()
        
        lblNoInvitatios?.text = NSLocalizedString("no_invitation", comment: "")
        lblNoInvitatios?.isHidden = true
    }
    
    func setupTableView() -> Void {
        // Helpers
        let attributes = [ NSForegroundColorAttributeName : Constants.baseColor ] as [String: Any]
        
        refreshControl.tintColor = Constants.baseColor
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("refresh_invitations", comment: ""), attributes: attributes)
        refreshControl.addTarget(self, action: #selector(refreshEvents(sender:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableInvitations?.refreshControl = refreshControl
        } else {
            tableInvitations?.addSubview(refreshControl)
        }
    }
    
    func refreshEvents(sender: UIRefreshControl) -> Void {
        invitationsPresenter.getInvitation()
    }
    
    @IBAction func acceptInvitation(sender: UIButton) -> Void {
        let parameters: JSON = ["state": true]
        invitationsPresenter.acceptInvitation(id: sender.tag, parameters: parameters)
    }
    
    @IBAction func cancelInvitation(sender: UIButton) -> Void {
        invitationsPresenter.cancelInvitation(id: sender.tag)
    }
}

extension InvitationsViewController: UITableViewDelegate {
    
}

extension InvitationsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return receipt.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvitationCell", for: indexPath) as! InvitationCell
        let invitation = receipt[indexPath.row]
        cell.name?.text = invitation.send.getFullName()
        cell.avatar?.af_setImage(withURL: invitation.send.getAvatarURL())
        cell.btnAccept?.titleLabel?.text = NSLocalizedString("accept_text", comment: "")
        cell.btnCancel?.titleLabel?.text = NSLocalizedString("cancel_text", comment: "")
        cell.btnCancel?.tag = invitation.id
        cell.btnAccept?.tag = invitation.id
        cell.btnAccept?.addTarget(self, action: #selector(acceptInvitation(sender:)), for: .touchUpInside)
        cell.btnCancel?.addTarget(self, action: #selector(cancelInvitation(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
}

extension InvitationsViewController: InvitationsView {
    func startLoading() {
        if !refreshControl.isRefreshing {
            KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
        }
    }
    
    func finishLoading() {
        refreshControl.endRefreshing()
        KRProgressHUD.dismiss()
    }
    
    func setInvitations(invitations: JSON) {
        self.receipt.removeAll()
        let receipts: [JSON] = invitations["receive"].arrayValue
        if (receipts.count > 0) {
            lblNoInvitatios?.isHidden = true
            tableInvitations?.isHidden = false
            for invitation: JSON in receipts {
                self.receipt.append(Invitation(json: invitation))
            }
            tableInvitations?.reloadData()
        } else {
            lblNoInvitatios?.isHidden = false
            tableInvitations?.isHidden = true
        }
    }
    
    func failRequest() {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_friends", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
