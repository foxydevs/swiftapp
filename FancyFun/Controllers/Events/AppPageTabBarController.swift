//
//  AppPageTabBarController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 26/07/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import Material

class AppPageTabBarController: PageTabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("menu_activities", comment: "")
    }
    
    open override func prepare() {
        super.prepare()
        
        delegate = self
        prepareTabBar()
    }
    
    
}

extension AppPageTabBarController {
    fileprivate func prepareTabBar() {
        pageTabBar.backgroundColor = Constants.tabBarBackgroud
        pageTabBar.lineColor = Constants.tabBarDivider
        pageTabBar.dividerColor = Constants.tabBarDivider
    }
}

extension AppPageTabBarController: PageTabBarControllerDelegate {
    func pageTabBarController(pageTabBarController: PageTabBarController, didTransitionTo viewController: UIViewController) {
        
    }
}
