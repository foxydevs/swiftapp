//
//  UserProfileViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 20/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import SwiftyJSON
import KRProgressHUD
import AlamofireImage
import SKPhotoBrowser

class UserProfileViewController: UIViewController {

    @IBOutlet weak var scrollBack: UIScrollView?
    @IBOutlet weak var avatar: UIImageView?
    @IBOutlet weak var lblDescription: UILabel?
    @IBOutlet weak var lblAddress: UILabel?
    @IBOutlet weak var lblLastConnection: UILabel?
    @IBOutlet weak var lblAge: UILabel?
    @IBOutlet weak var titleInterest: UILabel?
    @IBOutlet weak var titleFriends: UILabel?
    @IBOutlet weak var scrollInterests: UIScrollView?
    @IBOutlet weak var scrollFriends: UIScrollView?
    @IBOutlet weak var btnActions: UIButton?
    
    fileprivate let userProfilePresenter = UserProfilePresenter(userService: UserService(), friendsService: FriendsService())
    var user: User?
    var pictures = [SKPhoto]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userProfilePresenter.attachView(self)
        userProfilePresenter.getUser(id: (user?.id)!)
        scrollBack?.contentSize = CGSize(width: Constants.screenWidth, height: 850)
        
        let tapPhoto = UITapGestureRecognizer(target: self, action: #selector(loadImages(sender:)))
        tapPhoto.numberOfTapsRequired = 1
        tapPhoto.numberOfTouchesRequired = 1
        self.avatar?.isUserInteractionEnabled = true
        self.avatar?.addGestureRecognizer(tapPhoto)
    }

    func loadProfile() -> Void {
        avatar?.af_setImage(withURL: (user?.getAvatarURL())!)
        lblDescription?.text = user?.userDescription
        lblAddress?.text = user?.work
        lblLastConnection?.text = user?.last_connection
        lblAge?.text = user?.age
        self.title = user?.getFullName()
        
        let pictureFirst = SKPhoto.photoWithImageURL((user?.picture)!)
        pictureFirst.shouldCachePhotoURLImage = true
        pictures.append(pictureFirst)
        
        var xInterest = 10;
        for interest: Interest in (user?.interests)! {
            let labelInterest: UILabel = UILabel(frame: CGRect(x: xInterest, y: 20, width: 100, height: 50))
            labelInterest.text = interest.name
            labelInterest.backgroundColor = Constants.baseColor
            labelInterest.layer.cornerRadius = 8
            labelInterest.layer.masksToBounds = true
            labelInterest.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
            labelInterest.textAlignment = .center
            scrollInterests?.addSubview(labelInterest)
            xInterest = xInterest + 110
        }
        scrollInterests?.contentSize = CGSize(width:  xInterest + 5, height: 100)
        
        var xFriends = 10;
        for friend: User in (user?.similar)! {
            let viewFriend = UIView(frame: CGRect(x: xFriends, y: 0, width: 100, height: 120))
            let avatarFriend = UIImageView(frame: CGRect(x: 15, y: 0, width: 70, height: 70))
            avatarFriend.af_setImage(withURL: friend.getAvatarURL())
            avatarFriend.layer.cornerRadius = avatarFriend.frame.size.height/2
            avatarFriend.layer.masksToBounds = true
            let nameFriend = UILabel(frame: CGRect(x: 0, y: 70, width: 100, height: 50))
            nameFriend.text = friend.getFullName()
            nameFriend.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            nameFriend.textAlignment = .center
            nameFriend.numberOfLines = 2
            nameFriend.font = UIFont(name: "Helvetica Neue", size: 11)
            viewFriend.addSubview(avatarFriend)
            viewFriend.addSubview(nameFriend)
            scrollFriends?.addSubview(viewFriend)
            xFriends = xFriends + 110
        }
        
        scrollFriends?.contentSize = CGSize(width: xFriends + 10, height: 120)
        
        let isFriend = Singleton.shared.isFriend(user: user!)
        if isFriend {
            btnActions?.backgroundColor = Constants.baseColor
            btnActions?.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            btnActions?.setTitle(NSLocalizedString("text_deletefriend", comment: ""), for: .normal)
            btnActions?.addTarget(self, action: #selector(deleteFriend), for: .touchUpInside)
        } else {
            btnActions?.backgroundColor = Constants.facebookColor
            btnActions?.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            btnActions?.setTitle(NSLocalizedString("text_addfriend", comment: ""), for: .normal)
            btnActions?.addTarget(self, action: #selector(sendInvitation), for: .touchUpInside)
        }
        btnActions?.layer.cornerRadius = 10
        btnActions?.layer.masksToBounds = true
        
        if String(describing: user?.id) == Singleton.shared.userID {
            self.btnActions?.isHidden = true
        }
    }
    
    func sendInvitation() -> Void {
        let parameters: JSON = ["user_send": Singleton.shared.userID, "user_receipt": user?.id ?? ""]
        userProfilePresenter.sendInvitation(parameters: parameters)
    }
    
    func deleteFriend() -> Void {
        userProfilePresenter.deleteInvitation(user: Singleton.shared.userID, friend: (user?.id)!)
    }
    
    func loadImages(sender: UITapGestureRecognizer) -> Void {
        let browser = SKPhotoBrowser(photos: pictures)
        browser.initializePageIndex(0)
        self.present(browser, animated: true, completion: nil)
    }
    
}

extension UserProfileViewController: UserProfileView {
    func startLoading() {
        KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
    }
    
    func finishLoading() {
        KRProgressHUD.dismiss()
    }
    
    func reloadUser() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUser(user: JSON) {
        self.user = User(json: user)
        loadProfile()
    }
    
    func failRequest(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
