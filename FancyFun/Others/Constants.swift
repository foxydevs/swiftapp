//
//  Constants.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    //COLORS
    static let baseColor: UIColor = UIColor(red: 233/255.0, green: 30/255.0, blue: 99/255.0, alpha: 1.0)
    static let facebookColor: UIColor = UIColor(red: 59/255.0, green: 89/255.0, blue: 152/255.0, alpha: 1.0)
    static let primaryText: UIColor = UIColor.white
    static let menuTextColor: UIColor = UIColor(red: 114/255.0, green: 114/255.0, blue: 114/255.0, alpha: 1.0)
    static let tabBarBackgroud: UIColor = UIColor(red: 251/255.0, green: 252/255.0, blue: 252/255.0, alpha: 1.0)
    static let tabBarText: UIColor = UIColor(red: 98/255.0, green: 125/255.0, blue: 140/255.0, alpha: 1.0)
    static let tabBarDivider: UIColor = UIColor(red: 95/255.0, green: 126/255.0, blue: 146/255.0, alpha: 1.0)
    static let placeHolderText: UIColor = UIColor(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1.0)
    
    //PREFERENCES
    static let keyId: String = "id"
    static let keyFirstName: String = "firstname"
    static let keyLastName: String = "lastname"
    static let keyPicture: String = "picture"
    static let keyDistance: String = "distance"
    
    //SCREEN
    static let screenWidth: CGFloat = UIScreen.main.bounds.width
    static let screenHeight: CGFloat = UIScreen.main.bounds.height
    
    //URL API
    static let urlBase: String = "http://fancyfun.thefoxylabs.com/api"
    
    //USER SINGLETON
}

struct ButtonLayout {
    struct Fab {
        static let diameter: CGFloat = 48
    }
}
