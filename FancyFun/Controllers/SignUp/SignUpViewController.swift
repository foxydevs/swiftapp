//
//  SignUnViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftyJSON
import OneSignal
import FBSDKCoreKit
import FBSDKLoginKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var btnSignUp: UIButton?
    @IBOutlet weak var btnFacebook: UIButton?
    @IBOutlet weak var txtUsername: UITextField?
    @IBOutlet weak var txtPassword: UITextField?
    @IBOutlet weak var txtEmail: UITextField?
    
    fileprivate let signUpPresenter = SignUpPresenter(userService: UserService())
    fileprivate var oneSignalId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("HomeSignUp", comment: "")
        initializeLayout()
        
        signUpPresenter.attachView(self)
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        if let id = status.subscriptionStatus.userId {
            oneSignalId = id
        }
    }
    
    func initializeLayout() -> Void {
        
        self.setupButton(sender: btnSignUp!)
        self.setupButton(sender: btnFacebook!)
        self.txtUsername?.text = ""
        self.txtPassword?.text = ""
        self.txtEmail?.text = ""
        self.btnSignUp?.setTitle(NSLocalizedString("HomeSignUp", comment: ""), for: .normal)
        self.btnFacebook?.setTitle(NSLocalizedString("facebook_text", comment: ""), for: .normal)
        self.txtUsername?.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("username_text", comment: ""),
                                                               attributes: [NSForegroundColorAttributeName: Constants.placeHolderText])
        self.txtPassword?.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("password_text", comment: ""),
                                                                     attributes: [NSForegroundColorAttributeName: Constants.placeHolderText])
        self.txtEmail?.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("email_text", comment: ""),
                                                                     attributes: [NSForegroundColorAttributeName: Constants.placeHolderText])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupButton(sender: UIButton) -> Void {
        sender.layer.cornerRadius = 8
        sender.layer.borderColor = UIColor.white.cgColor
        sender.layer.borderWidth = 1
    }

    @IBAction func signUpAction(sender: UIButton) -> Void {
        if ((self.txtPassword?.text?.characters.count)! > 5) {
            if (((self.txtUsername?.text?.characters.count)! > 3) && ((self.txtEmail?.text?.characters.count)! > 4)) {
                let parameters: JSON = ["username": self.txtUsername?.text ?? "", "password": self.txtPassword?.text ?? "", "email": self.txtEmail?.text ?? "", "one_signal_id": oneSignalId]
                signUpPresenter.newUser(parameters: parameters)
            } else {
                let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_fields", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("alert_password", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
            
    func isPasswordValid(_ password : String) -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[A-Z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{5,}")
        return passwordTest.evaluate(with: password)
    }
    
    @IBAction func loginWithFacebook(_sender: UIButton) -> Void {
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil) {
                let fbLoginResult: FBSDKLoginManagerLoginResult = result!
                if (fbLoginResult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                }
            } else {
                print(error.debugDescription)
            }
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    let dataJSON = JSON(result ?? "")
                    let parameters: JSON = ["username": dataJSON["id"].stringValue, "facebook_id": dataJSON["id"].stringValue, "firstname": dataJSON["first_name"].stringValue, "lastname": dataJSON["last_name"].stringValue, "email": dataJSON["email"].stringValue, "one_signal_id": self.oneSignalId, "picture": dataJSON["picture"]["data"]["url"].stringValue]
                    self.signUpPresenter.doLoginFacebook(parameters: parameters)
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SignUpViewController: SignUpView {
    func startLoading() {
        KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
    }
    
    func finishLoading() {
        KRProgressHUD.dismiss()
    }
    
    func successSignUp() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func failSignUp() {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_login", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
