//
//  EventsViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 8/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftyJSON
import AlamofireImage
import Material

class EventsViewController: UIViewController {

    @IBOutlet weak var tableEvents: UITableView?
    let refreshControl = UIRefreshControl()
    
    var events: [Event] = []
    var friendEvents: Bool?
    fileprivate let eventsPresenter = EventsPresenter(eventsService: EventsService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableEvents?.dataSource = self
        tableEvents?.delegate = self
        setupTableView()
        
        eventsPresenter.attachView(self)
        if friendEvents! {
            eventsPresenter.getEventsFriends(parameters: ["id": Singleton.shared.userID])
        } else {
            KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
            Timer.scheduledTimer(withTimeInterval: 4, repeats: false, block: { (timer) in
                self.getEventsWhenLocation()
            })
        }
        
        let button = FABButton(image: Icon.cm.add, tintColor: .white)
        button.pulseColor = .white
        button.backgroundColor = Color.indigo.accent4
        button.addTarget(self, action: #selector(createEvent(sender:)), for: .touchUpInside)
        view.layout(button).width(ButtonLayout.Fab.diameter).height(ButtonLayout.Fab.diameter).bottomRight(bottom: 20, right: 10)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if Singleton.shared.reloadEvent {
            if friendEvents! {
                eventsPresenter.getEventsFriends(parameters: ["id": Singleton.shared.userID])
            } else {
                getEventsWhenLocation()
            }
            Singleton.shared.reloadEvent = false
        }
    }
    
    @IBAction func createEvent(sender: FABButton) -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewEventView") as! NewEventViewController
        vc.title = NSLocalizedString("newevent_text", comment: "")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupTableView() -> Void {
        // Helpers
        let attributes = [ NSForegroundColorAttributeName : Constants.baseColor ] as [String: Any]
        
        refreshControl.tintColor = Constants.baseColor
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("refresh_events", comment: ""), attributes: attributes)
        refreshControl.addTarget(self, action: #selector(refreshEvents(sender:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableEvents?.refreshControl = refreshControl
        } else {
            tableEvents?.addSubview(refreshControl)
        }
    }
    
    func refreshEvents(sender: UIRefreshControl) -> Void {
        if friendEvents! {
            eventsPresenter.getEventsFriends(parameters: ["id": Singleton.shared.userID])
        } else {
            getEventsWhenLocation()
        }
    }
    
    func getEventsWhenLocation() -> Void {
        let parameters: JSON = ["latitude": Singleton.shared.currentLocation?.coordinate.latitude ?? 0.0,"longitude": Singleton.shared.currentLocation?.coordinate.longitude ?? 0.0, "distance": Singleton.shared.distance]
        eventsPresenter.getNearEvents(parameters: parameters)
    }
    
    func setformats(date: String, time: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let dateC = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat = "d MMMM yyyy"
        let newDate = dateFormatter.string(from: dateC!)
        
        return "\(newDate) \(time)"
    }
}

extension EventsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventsCell", for: indexPath) as! EventsCell
        let shadowPath = UIBezierPath(roundedRect: (cell.shadowView?.bounds)!, cornerRadius: 2)
        
        cell.shadowView?.layer.masksToBounds = false
        cell.shadowView?.layer.cornerRadius = 5
        cell.shadowView?.layer.shadowColor = UIColor.black.cgColor
        cell.shadowView?.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.shadowView?.layer.shadowOpacity = 0.5
        cell.shadowView?.layer.shadowPath = shadowPath.cgPath
        
        let event = self.events[indexPath.row]
        cell.datetime?.text = setformats(date: event.date, time: event.time)
        cell.username?.text = event.user.getFullName()
        cell.avatar?.af_setImage(withURL: event.user.getAvatarURL())
        cell.picture?.image = nil
        if event.picture != "" {
            cell.picture?.af_setImage(withURL: event.getPictureURL())
        }
        cell.information?.text = event.description
        
        return cell
    }
}

extension EventsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SingleEventView") as! SingleEventViewController
        vc.event = self.events[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 370
    }
}

extension EventsViewController: EvenstView {
    func startLoading() {
        if !refreshControl.isRefreshing {
            KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
        }
    }
    
    func finishLoading() {
        refreshControl.endRefreshing()
        KRProgressHUD.dismiss()
    }
    
    func setEvents(events: [Event]) {
        self.events = events
        tableEvents?.reloadData()
    }
    
    func failRequest(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_events", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
