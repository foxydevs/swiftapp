//
//  FriendsListViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 12/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftyJSON
import AlamofireImage
import MessageUI

class FriendsListViewController: UIViewController {

    @IBOutlet weak var tableFriends: UITableView?
    @IBOutlet weak var lblNoFirends: UILabel?
    
    let refreshControl = UIRefreshControl()
    
    var friends: [User] = []
    var selectedRow = -1
    fileprivate let friendsListPresenter = FriendsListPresenter(friendsService: FriendsService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableFriends?.delegate = self
        tableFriends?.dataSource = self
        tableFriends?.tableFooterView = UIView(frame: .zero)
        setupTableView()
        friendsListPresenter.attachView(self)
        friendsListPresenter.getFriends(id: Singleton.shared.userID)
        
        lblNoFirends?.text = NSLocalizedString("no_friends", comment: "")
        lblNoFirends?.isHidden = true
    }
    
    func setupTableView() -> Void {
        // Helpers
        let attributes = [ NSForegroundColorAttributeName : Constants.baseColor ] as [String: Any]
        
        refreshControl.tintColor = Constants.baseColor
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("refresh_closer", comment: ""), attributes: attributes)
        refreshControl.addTarget(self, action: #selector(refreshEvents(sender:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableFriends?.refreshControl = refreshControl
        } else {
            tableFriends?.addSubview(refreshControl)
        }
    }
    
    func refreshEvents(sender: UIRefreshControl) -> Void {
        friendsListPresenter.getFriends(id: Singleton.shared.userID)
    }
    
    func sendMessage(sender: UIButton) -> Void {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = NSLocalizedString("message_fancy", comment: "")
            controller.recipients = [friends[sender.tag].phone]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func doCall(sender: UIButton) -> Void {
        if let url = URL(string: "tel://\(friends[sender.tag].phone)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func goUserProfile(sender: UIButton) -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileView") as! UserProfileViewController
        vc.user = friends[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToMap(sender: UIButton) -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapView") as! MapViewController
        vc.user = friends[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension FriendsListViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FriendsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsListCell", for: indexPath) as! FriendsListCell
        
        let friend = friends[indexPath.row]
        cell.lblName?.text = friend.getFullName()
        cell.lblDescription?.text = friend.userDescription
        cell.avatar?.af_setImage(withURL: friend.getAvatarURL())
        cell.btnMessage?.tag = indexPath.row
        cell.btnCall?.tag = indexPath.row
        cell.btnProfile?.tag = indexPath.row
        cell.btnMap?.tag = indexPath.row
        cell.btnMessage?.addTarget(self, action: #selector(sendMessage(sender:)), for: .touchUpInside)
        cell.btnCall?.addTarget(self, action: #selector(doCall(sender:)), for: .touchUpInside)
        cell.btnProfile?.addTarget(self, action: #selector(goUserProfile(sender:)), for: .touchUpInside)
        cell.btnMap?.addTarget(self, action: #selector(goToMap(sender:)), for: .touchUpInside)
        return cell
    }
}

extension FriendsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedRow == indexPath.row {
            return 155
        } else {
            return 105
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.beginUpdates()
        if selectedRow == indexPath.row {
            selectedRow = -1
        } else {
            selectedRow = indexPath.row
        }
        tableView.endUpdates()
    }
}

extension FriendsListViewController: FriendsListView {
    func startLoading() {
        if !refreshControl.isRefreshing {
            KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
        }
    }
    
    func finishLoading() {
        refreshControl.endRefreshing()
        KRProgressHUD.dismiss()
    }
    
    func setFriends(friends: JSON) {
        self.friends.removeAll()
        if (friends.count > 0) {
            lblNoFirends?.isHidden = true
            for friend: JSON in friends.arrayValue {
                self.friends.append(User(json: friend))
            }
            tableFriends?.reloadData()
        } else {
            lblNoFirends?.isHidden = false
        }
    }
    
    func failRequest() {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_friends", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
