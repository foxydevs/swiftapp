//
//  MenuCell.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 11/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView?
    @IBOutlet weak var name: UILabel?
    var iconName = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            name?.textColor = Constants.baseColor
            icon?.image = UIImage(named: iconName + "_active")
        } else {
            name?.textColor = Constants.menuTextColor
            icon?.image = UIImage(named: iconName + "_normal")
        }
    }

}
