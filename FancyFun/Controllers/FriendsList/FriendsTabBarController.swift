//
//  FriendsTabBarController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import Material

class FriendsTabBarController: PageTabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("menu_friendlist", comment: "")
    }
    
    open override func prepare() {
        super.prepare()
        
        delegate = self
        prepareTabBar()
    }
}

extension FriendsTabBarController {
    fileprivate func prepareTabBar() {
        pageTabBar.backgroundColor = Constants.tabBarBackgroud
        pageTabBar.lineColor = Constants.tabBarDivider
        pageTabBar.dividerColor = Constants.tabBarDivider
    }
}

extension FriendsTabBarController: PageTabBarControllerDelegate {
    func pageTabBarController(pageTabBarController: PageTabBarController, didTransitionTo viewController: UIViewController) {
        
    }
}
