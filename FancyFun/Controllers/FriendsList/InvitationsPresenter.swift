//
//  InvitationsPresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol InvitationsView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setInvitations(invitations: JSON)
    func failRequest()
}

class InvitationsPresenter {
    fileprivate let userService: UserService
    fileprivate let invitationService: FriendsService
    weak fileprivate var invitationView: InvitationsView?
    
    init(userService: UserService, invitationService: FriendsService) {
        self.userService = userService
        self.invitationService = invitationService
    }
    
    func attachView(_ view: InvitationsView) {
        invitationView = view
    }
    
    func detachView() {
        invitationView = nil
    }

    func getInvitation() -> Void {
        invitationView?.startLoading()
        userService.getInvitation(id: Singleton.shared.userID) { (invitations, status) in
            self.invitationView?.finishLoading()
            if status {
                self.invitationView?.setInvitations(invitations: invitations)
            } else {
                self.invitationView?.failRequest()
            }
        }
    }
    
    func acceptInvitation(id: Int, parameters: JSON) -> Void {
        invitationView?.startLoading()
        invitationService.acceptInvitation(id: id, parameters: parameters) { (invitation, status, message) in
            self.invitationView?.finishLoading()
            if status {
                self.getInvitation()
            } else {
                self.invitationView?.failRequest()
            }
        }
    }
    
    func cancelInvitation(id: Int) -> Void {
        invitationView?.startLoading()
        invitationService.cancelInvitation(id: id) { (invitation, status, message) in
            self.invitationView?.finishLoading()
            if status {
                self.getInvitation()
            } else {
                self.invitationView?.failRequest()
            }
        }
    }
}
