//
//  CloseFriendsCell.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 15/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit

class CloseFriendsCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblDescription: UILabel?
    @IBOutlet weak var lblAge: UILabel?
    @IBOutlet weak var btnDistance: UIButton?
    @IBOutlet weak var btnMessage: UIButton?
    @IBOutlet weak var btnInvitattion: UIButton?
    @IBOutlet weak var lblDistance: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
