//
//  CustomInterestCell.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 3/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit

class CustomInterestCell: UITableViewCell {

    @IBOutlet weak var name: UILabel?
    @IBOutlet weak var count: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
