//
//  FriendsListCell.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 16/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit

class FriendsListCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblDescription: UILabel?    
    @IBOutlet weak var btnMessage: UIButton?
    @IBOutlet weak var btnCall: UIButton?
    @IBOutlet weak var btnProfile: UIButton?
    @IBOutlet weak var btnMap: UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
