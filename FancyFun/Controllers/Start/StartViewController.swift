//
//  StartViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    @IBOutlet weak var btnSignIn: UIButton?
    @IBOutlet weak var btnSignUp: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btnSignIn?.setTitle(NSLocalizedString("HomeSignIn", comment: ""), for: .normal)
        btnSignUp?.setTitle(NSLocalizedString("HomeSignUp", comment: ""), for: .normal)
        
        self.setupButton(sender: btnSignUp!)
        self.setupButton(sender: btnSignIn!)
    }
    
    func setupButton(sender: UIButton) -> Void {
        sender.layer.cornerRadius = 8
        sender.layer.borderColor = UIColor.white.cgColor
        sender.layer.borderWidth = 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let preferences = UserDefaults.standard
        if preferences.object(forKey: Constants.keyId) != nil {
            Singleton.shared.loadUser()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CenterView") as! CenterViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func goToSignIn(sender: UIButton) -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInView") as! SignInViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func goToSignUp(sender: UIButton) -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpView") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
