//
//  NewEventViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 15/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import SwiftyJSON
import GooglePlacePicker
import KRProgressHUD

class NewEventViewController: UIViewController {

    @IBOutlet weak var scrollBack: UIScrollView?
    @IBOutlet weak var txtDate: DateFieldTextField?
    @IBOutlet weak var txtHour: DateFieldTextField?
    @IBOutlet weak var txtPlace: UITextField?
    @IBOutlet weak var txtAddress: UITextField?
    @IBOutlet weak var txtDescription: UITextField?
    @IBOutlet weak var lblPlace: UILabel?
    @IBOutlet weak var pictureEvent: UIImageView?
    
    var eventUpdate: Event?
    fileprivate var googlePlace: GMSPlace?
    fileprivate var havePicture = false
    fileprivate let newEventsPresenter = NewEventPresenter(eventsService: EventsService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadLayout()
        
        newEventsPresenter.attachView(self)
    }
    
    func loadLayout() -> Void {
        scrollBack?.contentSize = CGSize(width: Constants.screenWidth, height: 650)
        txtDate?.placeholder = NSLocalizedString("date_placeholder", comment: "")
        txtHour?.placeholder = NSLocalizedString("hour_placeholder", comment: "")
        txtPlace?.placeholder = NSLocalizedString("place_placeholder", comment: "")
        txtAddress?.placeholder = NSLocalizedString("address_placeholder", comment: "")
        txtDescription?.placeholder = NSLocalizedString("description_placeholder", comment: "")
        lblPlace?.text = NSLocalizedString("selectplace_text", comment: "")
        
        
        txtDate?.dateDelegate = self
        txtDate?.datetype = SCDatePicker.SCDatePickerType.date
        txtHour?.dateDelegate = self
        txtHour?.datetype = SCDatePicker.SCDatePickerType.time
        
        if eventUpdate == nil {
            let itemCreate: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("create_text", comment: ""), style: .plain, target: self, action: #selector(createEvent(sender:)))
            self.navigationItem.rightBarButtonItem = itemCreate
        } else {
            let itemCreate: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("update_text", comment: ""), style: .plain, target: self, action: #selector(createEvent(sender:)))
            self.navigationItem.rightBarButtonItem = itemCreate
            
            txtDate?.text = eventUpdate?.date
            txtHour?.text = eventUpdate?.time
            txtPlace?.text = eventUpdate?.place
            txtAddress?.text = eventUpdate?.address
            txtDescription?.text = eventUpdate?.description
            pictureEvent?.af_setImage(withURL: (eventUpdate?.getPictureURL())!)
        }
        
    }
    
    @IBAction func createEvent(sender: UIBarButtonItem) -> Void{
        let date = txtDate?.text ?? ""
        let time = txtHour?.text ?? ""
        let place = txtPlace?.text ?? ""
        let address = txtAddress?.text ?? ""
        let description = txtDescription?.text ?? ""
        var latitude = 0.0
        var longitude = 0.0
        var placeId = ""
        if googlePlace != nil {
            latitude = googlePlace?.coordinate.latitude ?? 0.0
            longitude = googlePlace?.coordinate.longitude ?? 0.0
            placeId = googlePlace?.placeID ?? ""
        } else {
            if eventUpdate != nil {
                latitude = (eventUpdate?.latitude)!
                longitude = (eventUpdate?.longitude)!
                placeId = (eventUpdate?.place_id)!
            }
        }
        
        
        
        if date != "" && time != "" && description != ""{
            if description.characters.count > 10 {
                if (havePicture || eventUpdate != nil) {
                    let parameters: JSON = ["date": date, "time": time, "place": place, "address": address, "description": description, "latitude": latitude, "longitude": longitude, "user_created": Singleton.shared.userID, "place_id": placeId]
                    
                    if eventUpdate == nil {
                        newEventsPresenter.createEvent(parameters: parameters)
                    } else {
                        newEventsPresenter.updateEvent(id: (eventUpdate?.id)!, parameters: parameters)
                    }
                    
                } else {
                    //DEBE COLOCAR UNA FOTO
                    let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_picture", comment: ""), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                //LA DESCRIPCION DEBE DE SER MAYOR A 10
                let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_description", comment: ""), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)

            }
        } else {
            //DEBE LLENAR LOS CAMPOS
            let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_fields", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)

        }
    }
    
    @IBAction func searchPlace(sender: UIButton) -> Void {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        self.present(placePicker, animated: true, completion: nil)
    }
    
    func loadPlacePicture(place: GMSPlace) {
        GMSPlacesClient.shared().lookUpPhotos(forPlaceID: place.placeID) { (photos, error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            } else {
                if let firstPhoto = photos?.results.first {
                    GMSPlacesClient.shared().loadPlacePhoto(firstPhoto, callback: { (photo, error) in
                        if let error = error {
                            print("Error: \(error.localizedDescription)")
                        } else {
                            self.pictureEvent?.image = photo
                            self.havePicture = true
                        }
                    })
                }
            }
        }
    }
    
    @IBAction func takePicture(sender: UIButton) -> Void {
        let actionSheetController = UIAlertController(title: NSLocalizedString("option_text", comment: ""), message: NSLocalizedString("takepicture_text", comment: ""), preferredStyle: .actionSheet)
        let optionCamera = UIAlertAction(title: NSLocalizedString("camera_text", comment: ""), style: .default) { (UIAlertAction) in
            self.openCamera()
        }
        
        let optionGallery = UIAlertAction(title: NSLocalizedString("camera_gallery", comment: ""), style: .default) { (UIAlertAction) in
            self.openGallery()
        }
        
        let optionCancel = UIAlertAction(title: NSLocalizedString("cancel_text", comment: ""), style: .default) { (UIAlertAction) in
            
        }
        
        actionSheetController.addAction(optionCamera)
        actionSheetController.addAction(optionGallery)
        actionSheetController.addAction(optionCancel)
        
        
        if let popoverController = actionSheetController.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func openCamera() -> Void {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery() -> Void {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true)
    }
}

extension NewEventViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.dismiss(animated: true, completion: nil)
            pictureEvent?.image = image
            havePicture = true
        }
    }
}

extension NewEventViewController: SCDateFieldDelegate {
    func dateTextHasChanged(dateString: String, date: NSDate) {
        print(dateString)
    }
}

extension NewEventViewController: GMSPlacePickerViewControllerDelegate {
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        googlePlace = place
        txtPlace?.text = place.name
        txtAddress?.text = place.formattedAddress
        loadPlacePicture(place: place)
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}

extension NewEventViewController: NewEventView {
    func startLoading() {
        KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
    }
    
    func finishLoading() {
        KRProgressHUD.dismiss()
    }
    
    func finishUploadPicture() {
        Singleton.shared.reloadEvent = true
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setEvent(event: JSON) {
        KRProgressHUD.update(text: NSLocalizedString("loading_picture", comment: ""))
        newEventsPresenter.uploadPicture(id: event["id"].intValue, photo: (self.pictureEvent?.image)!)
    }
    
    func updateEvent(event: JSON) {
        if havePicture {
            KRProgressHUD.update(text: NSLocalizedString("loading_picture", comment: ""))
            newEventsPresenter.uploadPicture(id: event["id"].intValue, photo: (self.pictureEvent?.image)!)
        } else {
            KRProgressHUD.dismiss()
            Singleton.shared.reloadEvent = true
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    func failRequest(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
