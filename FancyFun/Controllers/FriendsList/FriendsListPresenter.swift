//
//  FriendsListPresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 16/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol FriendsListView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setFriends(friends: JSON)
    func failRequest()
}

class FriendsListPresenter {
    fileprivate let friendsService: FriendsService
    weak fileprivate var friendsListView: FriendsListView?
    
    init(friendsService: FriendsService) {
        self.friendsService = friendsService
    }
    
    func attachView(_ view: FriendsListView) {
        friendsListView = view
    }
    
    func detachView() {
        friendsListView = nil
    }
    
    func getFriends(id: String) {
        friendsListView?.startLoading()
        friendsService.getFriends(id: id) { (friends, status) in
            self.friendsListView?.finishLoading()
            if status {
                self.friendsListView?.setFriends(friends: friends)
            } else {
                self.friendsListView?.failRequest()
            }
        }
    }
}
