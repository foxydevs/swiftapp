//
//  RestClient.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 7/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

protocol RestClientDelegate {
    func endPointResponse(endpoint: String, response: JSON, status: Bool)
}

class RestClient {
    var delegate: RestClientDelegate?
    var urlBase: String = "http://52.89.117.243/api"
    var tag: String?
    let endPoints: JSON = [
        "login": "/login"
    ]
    
    init(tag: String) {
        self.tag = tag
    }
    
    func doLogin(parameters: JSON) -> Void {
        let urlString: String = urlBase + endPoints["login"].stringValue;
        Alamofire.request(urlString, method: .post, parameters: parameters.dictionaryObject, encoding: JSONEncoding.default)
            .response { (response) in
                let json = JSON(data: response.data!)
                
        }
    }
}
