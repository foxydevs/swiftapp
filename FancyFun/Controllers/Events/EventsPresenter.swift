//
//  EventsPresenter.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 8/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol EvenstView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setEvents(events: [Event])
    func failRequest(message: String)
}

class EventsPresenter {
    fileprivate let eventsService: EventsService
    weak fileprivate var eventsView: EvenstView?
    
    init(eventsService: EventsService) {
        self.eventsService = eventsService
    }
    
    func attachView(_ view: EvenstView) {
        eventsView = view
    }
    
    func detachView() {
        eventsView = nil
    }
    
    func getNearEvents(parameters: JSON) {
        eventsView?.startLoading()
        eventsService.getNearEvents(parameters: parameters) { (events, status) in
            self.eventsView?.finishLoading()
            if status {
                var eventsReturn: [Event] = []
                for event: JSON in events.arrayValue{
                    eventsReturn.append(Event(json: event))
                }
                self.eventsView?.setEvents(events: eventsReturn)
            } else {
                self.eventsView?.failRequest(message: "Error get Events")
            }
        }
    }
    
    func getEventsFriends(parameters: JSON) {
        eventsView?.startLoading()
        eventsService.getEventsFriends(parameters: parameters) { (events, status, message) in
            self.eventsView?.finishLoading()
            if status {
                var eventsReturn: [Event] = []
                for event: JSON in events.arrayValue{
                    eventsReturn.append(Event(json: event))
                }
                self.eventsView?.setEvents(events: eventsReturn)
            } else {
                self.eventsView?.failRequest(message: message)
            }
        }
    }
}
