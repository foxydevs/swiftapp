//
//  Event.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 10/08/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import Foundation
import SwiftyJSON

class Event {
    var id: Int
    var date: String
    var time: String
    var place_id: String
    var place: String
    var address: String
    var picture: String
    var description: String
    var latitude: Double
    var longitude: Double
    var user_created: Int
    var created_at: String
    var updated_at: String
    var distance: Float
    var user: User
    var assistants: [UserEvent] = []
    var interested: [UserEvent] = []
    var advertisings: [Advertising] = []
    
    init(json: JSON) {
        id = json["id"].intValue
        date = json["date"].stringValue
        time = json["time"].stringValue
        place_id = json["place_id"].stringValue
        place = json["place"].stringValue
        address = json["address"].stringValue
        picture = json["picture"].stringValue
        description = json["description"].stringValue
        latitude = json["latitude"].doubleValue
        longitude = json["longitude"].doubleValue
        user_created = json["user_created"].intValue
        created_at = json["created_at"].stringValue
        updated_at = json["updated_at"].stringValue
        distance = json["distance"].floatValue
        user = User(json: json["user"])
        
        for assist: JSON in json["assistants"].arrayValue {
            assistants.append(UserEvent(json: assist))
        }
        
        for interest: JSON in json["interested"].arrayValue {
            interested.append(UserEvent(json: interest))
        }
        
        for advertising: JSON in json["advertising"].arrayValue {
            advertisings.append(Advertising(json: advertising))
        }
    }
    
    func getPictureURL() -> URL {
        return URL(string: self.picture)!
    }
}
