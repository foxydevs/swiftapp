//
//  ProfileViewController.swift
//  FancyFun
//
//  Created by Alex Mejicanos on 12/06/17.
//  Copyright © 2017 GTechngology. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftyJSON

class ProfileViewController: UIViewController {

    @IBOutlet weak var cardUser: UIView?
    @IBOutlet weak var cardEvents: UIView?
    @IBOutlet weak var pictureUser: UIImageView?
    @IBOutlet weak var userName: UILabel?
    @IBOutlet weak var userWork: UILabel?
    @IBOutlet weak var userAge: UILabel?
    @IBOutlet weak var eventsCreated: UILabel?
    @IBOutlet weak var eventsAssisted: UILabel?
    @IBOutlet weak var titleCreated: UILabel?
    @IBOutlet weak var titleAssisted: UILabel?
    @IBOutlet weak var buttonInterests: UIButton?
    @IBOutlet weak var tableInterest: UITableView?
    
    fileprivate let profilePresenter = ProfilePresenter(userService: UserService())
    fileprivate var currentUser: User = User(json: [:])
    fileprivate var userInterest: [Interest] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        configureShadow()
        tableInterest?.delegate = self
        tableInterest?.dataSource = self
        profilePresenter.attachView(self)
        
        let buttonEdit: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_edit"), style: .plain, target: self, action: #selector(goToEdit(sender:)))
        self.navigationItem.rightBarButtonItem = buttonEdit
        
        let tapPicture = UITapGestureRecognizer(target: self, action: #selector(changePicture(sender:)))
        tapPicture.numberOfTapsRequired = 1
        tapPicture.numberOfTouchesRequired = 1
        pictureUser?.isUserInteractionEnabled = true
        pictureUser?.addGestureRecognizer(tapPicture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        profilePresenter.getUser(id: Singleton.shared.userID)
    }
    
    func loadUser() {
        if currentUser.picture != "" {
            pictureUser?.af_setImage(withURL: URL(string: currentUser.picture)!)
        }
        userName?.text = "\(String(describing: currentUser.firstname)) \(String(describing: currentUser.lastname))"
        userWork?.text = currentUser.work
        userAge?.text = currentUser.age
        
        eventsCreated?.text = "\(currentUser.numberEventsCreated) \(NSLocalizedString("menu_activities", comment: ""))"
        eventsAssisted?.text = "\(currentUser.numberEventsAssisted) \(NSLocalizedString("menu_activities", comment: ""))"
        
        if (currentUser.interests.count > 0) {
            userInterest = currentUser.interests
        } else {
            userInterest.removeAll()
            userInterest.append(Interest(json: ["name": NSLocalizedString("add_interest", comment: "")]))
        }
        
        tableInterest?.reloadData()
    }
    
    func configureShadow() -> Void {
        let shadowPathUser = UIBezierPath(roundedRect: (cardUser?.bounds)!, cornerRadius: 2)
        
        cardUser?.layer.masksToBounds = false
        cardUser?.layer.cornerRadius = 5
        cardUser?.layer.shadowColor = UIColor.black.cgColor
        cardUser?.layer.shadowOffset = CGSize(width: 0, height: 3)
        cardUser?.layer.shadowOpacity = 0.5
        cardUser?.layer.shadowPath = shadowPathUser.cgPath
        
        let shadowPathEvents = UIBezierPath(roundedRect: (cardEvents?.bounds)!, cornerRadius: 2)
        
        cardEvents?.layer.masksToBounds = false
        cardEvents?.layer.cornerRadius = 5
        cardEvents?.layer.shadowColor = UIColor.black.cgColor
        cardEvents?.layer.shadowOffset = CGSize(width: 0, height: 3)
        cardEvents?.layer.shadowOpacity = 0.5
        cardEvents?.layer.shadowPath = shadowPathEvents.cgPath
        
        userName?.text = "Fancy Fun"
        userWork?.text = "Facebook"
        userAge?.text = "24"
        eventsCreated?.text = "0 \(NSLocalizedString("menu_activities", comment: ""))"
        eventsAssisted?.text = "0 \(NSLocalizedString("menu_activities", comment: ""))"
        titleCreated?.text = NSLocalizedString("events_created", comment: "")
        titleAssisted?.text = NSLocalizedString("events_assisted", comment: "")
        buttonInterests?.titleLabel?.text = NSLocalizedString("text_viewinterest", comment: "")
        
        pictureUser?.layer.cornerRadius = (pictureUser?.frame.size.height)!/2
        pictureUser?.layer.masksToBounds = true
    }
    
    func goToEdit(sender: UIBarButtonItem) -> Void {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfile") as! EditProfileViewController
        vc.userUpdate = self.currentUser
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func changePicture(sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: NSLocalizedString("option_text", comment: ""), message: NSLocalizedString("takepicture_text", comment: ""), preferredStyle: .actionSheet)
        
        let optionCamera = UIAlertAction(title: NSLocalizedString("camera_text", comment: ""), style: .default) { (UIAlertAction) in
            self.openCamera()
        }
        
        let optionGallery = UIAlertAction(title: NSLocalizedString("camera_gallery", comment: ""), style: .default) { (UIAlertAction) in
            self.openGallery()
        }
        
        let optionCancel = UIAlertAction(title: NSLocalizedString("cancel_text", comment: ""), style: .cancel) { (UIAlertAction) in
            
        }
        
        alert.addAction(optionCamera)
        alert.addAction(optionGallery)
        alert.addAction(optionCancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() -> Void {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery() -> Void {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true)
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.dismiss(animated: true, completion: nil)
            pictureUser?.image = image
            profilePresenter.uploadPicture(id: Singleton.shared.userID, photo: image)
        }
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userInterest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InterestCell", for: indexPath) as! InterestCell
        let interest: Interest = userInterest[indexPath.row]
        cell.name?.text = interest.name
        cell.name?.textColor = UIColor(red: 100/255.0, green: 100/255.0, blue: 100/255.0, alpha: 1.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("text_interest", comment: "")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InterestView") as! InterestViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ProfileViewController: ProfileView {
    func startLoading() {
        KRProgressHUD.show(progressHUDStyle: .black, message: NSLocalizedString("loading", comment: ""))
    }
    
    func finishLoading() {
        KRProgressHUD.dismiss()
    }
    
    func uploadPicture() {
        
    }
    
    func setUser(user: User) {
        self.currentUser = user
        loadUser()
    }
    
    
    func failRequest(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("appname", comment: ""), message: NSLocalizedString("error_user", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok_text", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
